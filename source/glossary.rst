Glossary
===========

Acronyms
----------

.. glossary::
    BR
        a branching ratio

    EW
        electroweak

    FKS
        the Frixione-Kunszt-Signer scheme used in McMule. See Section :ref:`sec_fks`.

    FSR
        final state radiation

    IR
        infra-red

    HVP
        hadronic vacuum polarisation

    ISR
        initial state radiation

    LO
        leading order

    LP
        leading power

    NLO
        next-to-leading order

    NLP
        next-to-leading power

    NNLO
        next-to-next-to-leading order

    NTS
        next-to-soft

    OS
        on-shell renormalisation scheme in which the masses correspond to the poles of the propagators and :math:`\alpha=\alpha(q^2=0)` in the Thomson limit

    PCS
        pseudo-collinear sinuglarities, the numerical instabilties in

        .. math::
           \mathcal{M}_{n+1}^{(\ell )}
              \propto \frac{1}{q\cdot k}
              = \frac1{\xi^2}\frac1{1-y\beta}

        where :math:`y` is the angle between photon (:math:`k`) and electron (:math:`q`).
        For large velocities :math:`\beta` (or equivalently small masses), this becomes almost singular as :math:`y\to1`.

    PID
        particle identification, the ordering of particles in the code

    RNG
        random number generator, used to generate pseudo-random numbers for the Monte Carlo generation. See Section :ref:`sec_rng`

    SHA1
        secure-hasing-algorithm-1, used for hashing McMule's source code in autoversioning

    SM
        the Standard Model of particle physics

    VP
        vacuum polarisation

Technical terms
----------------

.. glossary::

    config file
        a shell file specifying, among other things, the statistics to be used

    containerisation
        the concept of bundling all dependecies etc. with McMule. See Sections :ref:`sec_docker` and :ref:`sec_docker`

    container
        a container that has bundled all dependecies etc. with McMule. See Sections :ref:`sec_docker` and :ref:`sec_docker`

    corner region
        a region of phase space where the mapping defined in Section :ref:`sec_ps` is not unique.
        The corner region refers to the smaller part of this double mapping.

    counter-event
        the soft event that gets subtracted in FKS, cf. :eq:`eq_event_counterevent`

    event
        the hard event that does not get subtracted in FKS, cf. :eq:`eq_event_counterevent`

    full period
        a surjective :term:`RNG`

    generic pieces
        a generic piece describes a part of the calculation such as the real or virtual corrections that themselves may be further subdivided as is convenient.

    generic processes
        A generic process is a prototype for the physical process such as :math:`\ell p\to\ell p` where the flavour of the lepton :math:`\ell` is left open.

    menu file
        A menu file contains a list of jobs to be computed s.t. the user will only have to vary the random seed and :math:`\xi_{c}` by hand as the statistical requirements are defined globally in a :term:`config file`.

    measurement function
        A function that takes as arguments the four-momenta of all particles involved in the reaction and returns the experimentally measured quantity.

    process group
        Processes are grouped into process grous if they share matrix elements such as :math:`\mu\to\nu\bar\nu e` and :math:`\mu\to\nu\bar\nu e\gamma` (``mudec``) or :math:`e\mu\to e\mu` and :math:`\ell p\to\ell p` (``mue``).

    random seed
        the initial value of the :term:`RNG`. In McMule this may be between 1 and 2<sup>31</sup>-2. See Section :ref:`sec_rng` for further details.

    soft cut
        a value of :math:`\xi` below which no subtraction takes place and the integrand is set to zero

    submission script
        a script that is provided by pymule to run a :term:`menu file`.
