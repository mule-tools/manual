import os
import matplotlib
matplotlib.use('Agg')



## LaTeX begins in the next line
from pymule import *

# To normalise branching ratios, we need the tau lifetime
lifetime = 1/(1000*(6.582119e-25)/(2.903e-13))

# The folder where McMule has stored the statefiles
setup(folder='example2/out.tar.bz2')

# Import LO data and re-scale to branching ratio
LO = scaleset(mergefks(sigma('m2enng0')), GF**2*lifetime*alpha)

# Import NLO corrections from the three pieces
NLO = scaleset(mergefks(
    sigma('m2enngR'),      # real corrections
    sigma('m2enngC'),     # counter term
    anyxi=sigma('m2enngV') # virtual corrections
), GF**2*lifetime*alpha**2)

# The branching ratio at NLO = LO + correction
fullNLO = plusnumbers(LO['value'], NLO['value'])

# Print results
print("BR_0 = ", printnumber(LO['value']))
print("dBR  = ", printnumber(NLO['value']))

# Produce energy plot
fig1, (ax1, ax2) = kplot(
    {'lo': LO['Ee'], 'nlo': NLO['Ee']},
    labelx=r"$E_e\,/\,{\rm MeV}$",
    labelsigma=r"$\D\mathcal{B}/\D E_e$"
)
ax2.set_ylim(-0.2,0.01)

# Produce visible mass plot
fig2, (ax1, ax2) = kplot(
    {'lo': LO['minv'], 'nlo': NLO['minv']},
    labelx=r"$m_{e\gamma}\,/\,{\rm MeV}$",
    labelsigma=r"$\D\mathcal{B}/\D m_{e\gamma}$"
)
ax1.set_yscale('log')
ax1.set_xlim(1000,0) ; ax1.set_ylim(5e-9,1e-3)
ax2.set_ylim(-0.2,0.)
# LaTeX ends here

bexp = np.array([1.85e-2, 0.05e-2])
print("Tension to the experimental value: ",np.sqrt(chisq([bexp, fullNLO])))

import pymule.plot,re

fig1.savefig('tau:energy.svg')
fig2.savefig('tau:minv.svg')


## LaTeX begins in the next line
# Loading the LO is the same as before
setup(folder='example3/out.tar.bz2')
LO = scaleset(mergefks(sigma('m2enng0')), GF**2*lifetime*alpha)

# Import the excl. NLO by specifying the observable
# for the real corrections
NLOexcl = scaleset(mergefks(
    sigma('m2enngR', obs='e'),
    sigma('m2enngC'),
    anyxi=sigma('m2enngV')
), GF**2*lifetime*alpha**2)
fullNLOexcl = addsets([LO, NLOexcl])

NLOincl = scaleset(mergefks(
    sigma('m2enngR', obs='i'),
    sigma('m2enngC'),
    anyxi=sigma('m2enngV')
), GF**2*lifetime*alpha**2)
fullNLOincl = addsets([LO, NLOincl])

print("BR_0 = ", printnumber(LO['value']))
print("BRexcl = ", printnumber(fullNLOexcl['value']))
print("BRincl = ", printnumber(fullNLOincl['value']))

fig3, (ax1, ax2) = kplot(
    {
        'lo': scaleplot(LO['Ee'], 1e3),
        'nlo': scaleplot(NLOexcl['Ee'], 1e3),
        'nlo2': scaleplot(NLOincl['Ee'], 1e3)
    },
    labelx=r"$E_e\,/\,{\rm GeV}$",
    labelsigma=r"$\D\mathcal{B}/\D E_{e}$",
    legend={
        'lo': '$\\rm LO$',
        'nlo': '$\\rm NLO\ exclusive$',
        'nlo2': '$\\rm NLO\ inclusive$'
    },
    legendopts={'what': 'l', 'loc': 'lower left'}
)
ax2.set_ylim(-0.12,0.02)

# LaTeX ends here
fig3.savefig('tau:boost.svg')
