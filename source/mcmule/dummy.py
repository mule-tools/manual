from pymule import *

lifetime = 1/(1000*(6.582119e-25)/(2.903e-13))
# define vegas directory
setup(folder="example1/")
dat = scaleset(
    mergefks(sigma("m2enng0")),
    GF**2*alpha*lifetime
)

dat.keys()
# dict_keys(['time', 'chi2a', 'value', 'Ee', 'minv'])

from matplotlib.pyplot import *
fig = plt.figure()
errorband(dat['Ee'])
plt.ylabel(r'$\D\mathcal{B}/\D E_e$')
plt.xlabel(r'$E_e\,/\,{\rm MeV}$')
mulify(fig)
fig.savefig("dummy.svg")
