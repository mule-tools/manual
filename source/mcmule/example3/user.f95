                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use mcmule
  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nrq = 4
  integer, parameter :: nrbins = 90
  real:: &
     min_val(nr_q) = (/    0.,   0., -0.8, -0.8 /)
  real:: &
     max_val(nr_q) = (/ 1800., 4500.,  1.0,  1.0 /)
  integer :: userdim = 0
  real(kind=prec) :: cth
  integer exclusiveQ
  real(kind=prec), parameter :: Etau = 10.58e3 / 2.

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer :: namesLen=6
  integer :: filenamesuffixLen=10
  integer :: nq = nrq
  integer :: nbins = nrbins


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU

  musq = mM**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  read*, exclusiveQ

  if(exclusiveQ == 1) then
    print*, "Calculating tau->e nu nu gamma in ee->tau tau exclusive"
    filenamesuffix = "e"
  else
    print*, "Calculating tau->e nu nu gamma in ee->tau tau inclusive"
    filenamesuffix = "i"
  endif

  ! Let the tau be unpolarised
  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: ptau, cos_e, cos_g
  real (kind=prec) :: p1Lab(4), p2Lab(4), p5Lab(4), p6Lab(4)
  real (kind=prec) :: quant(nr_q)
  real (kind=prec) :: gs(4), gh(4)
  real (kind=prec), parameter :: ez(4) = (/ 0., 0., 1., 0. /)
  !! ==== keep the line below in any case ==== !!
  call fix_mu
  pass_cut = .true.

  ptau = sqrt(Etau**2-Mtau**2)
  p1Lab = (/ 0., ptau*sqrt(1-cth**2), ptau*cth, Etau /)

  p1Lab = boost_back(p1Lab, q1)
  p2Lab = boost_back(p1Lab, q2)
  p5Lab = boost_back(p1Lab, q5)
  p6Lab = boost_back(p1Lab, q6)

  if (p5Lab(4) > p6Lab(4)) then
    gh = p5Lab ; gs = p6Lab
  else
    gh = p6Lab ; gs = p5Lab
  endif

  cos_e = cos_th(p2Lab, ez)
  cos_g = cos_th(gh   , ez)

  if ( (cos_e > 0.95 .or. cos_e < -0.75) .or. (cos_g > 0.95 .or. cos_g < -0.75) ) then
    pass_cut = .false.
    return
  endif

  if(exclusiveQ == 1) then
    if (gh(4) < 220. .or. gs(4) > 220.) then
      pass_cut = .false.
      return
    endif
  else
    if (gh(4) < 220.) then
      pass_cut = .false.
      return
    endif
  endif

  names(1) = 'minv'
  quant(1) = sqrt(sq(q2+gh))

  names(2) = 'Ee'
  quant(2) = p2Lab(4)

  names(3) = 'cos_e'
  quant(3) = cos_e

  names(4) = 'cos_g'
  quant(4) = cos_g
  END FUNCTION QUANT


  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)

  cth = 2*x(1) - 1
  userweight = (1+Mm**2/Etau**2) + (1-Mm**2/Etau**2) * cth
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
