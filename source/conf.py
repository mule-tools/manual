import sys
import os
sys.path.insert(0, os.path.abspath("./_ext"))

import unittest.mock as mock

MOCK_MODULES = [
    'cycler',
    'mesonbuild',
    'mesonbuild.scripts.depscan',
    'mesonbuild.mesonmain',
    'matplotlib',
    'matplotlib.pyplot',
    'matplotlib.colors',
    'matplotlib.patches',
    'matplotlib.path',
    'matplotlib.collections',
    'matplotlib.lines',
    'mpl_toolkits',
    'mpl_toolkits.axes_grid1',
    'mpl_toolkits.axes_grid1.mpl_axes',
    'numpy',
    'numpy.typing',
    'scipy',
    'scipy.stats',
]
for mod_name in MOCK_MODULES:
    sys.modules[mod_name] = mock.Mock()


# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'McMule'
copyright = '2023, The McMule Collaboration'
author = 'The McMule Collaboration'
release = 'v0.5.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
    'sphinx.ext.autodoc',
    'sphinxcontrib.mermaid',
    'sphinxcontrib.rsvgconverter',
    'sphinxfortran.fortran_domain',
    'pid',
    'authors',
    'sphinxcontrib.bibtex'
]
numfig_format = {'figure': 'Figure %s'}
bibtex_bibfiles = ['reference.bib']
bibtex_default_style = "plain"

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '.venv']
numfig = True

mermaid_version = "9.4.2-rc.1"


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
using_rtd_theme = True
html_static_path = ['_static']

latex_elements = {
    "fncychap": "",
    'preamble': r"""
    \DeclareUnicodeCharacter{03B1}{$\alpha$}
    \DeclareUnicodeCharacter{03B3}{$\gamma$}
    \DeclareUnicodeCharacter{03B4}{$\delta$}
    \DeclareUnicodeCharacter{03B5}{$\epsilon$}
    \DeclareUnicodeCharacter{03BC}{$\mu$}
    \DeclareUnicodeCharacter{03C0}{$\pi$}
    \DeclareUnicodeCharacter{03C4}{$\tau$}
    \DeclareUnicodeCharacter{207A}{$^+$}
    """
}
