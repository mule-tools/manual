.. _sec_pid:

Particle ID
===========

The following table lists the :f:var:`which_pieces` of McMule as well as the corresponding :term:`PID`.
For example, when calculating the process :math:`\mu^+\to e^+\nu\bar\nu e^+e^-`, the measurement function may receive up to seven arguments that can be mapped to particles as follows:

.. code:: fortran

    FUNCTION QUANT(Q1,Q2,Q3,Q4,Q5,Q6,Q7)
    real(kind=prec) :: q1(4) ! incoming muon+
    real(kind=prec) :: q2(4) ! outgoing electron+
    real(kind=prec) :: q3(4) ! outgoing neutrino, averaged over
    real(kind=prec) :: q4(4) ! outgoing neutrino, averaged over
    real(kind=prec) :: q5(4) ! outgoing electron-
    real(kind=prec) :: q6(4) ! outgoing electron+
    real(kind=prec) :: q7(4) ! outgoing optional photon

    pol1 = (/ 0., 0., -0.85, 0. /) ! set incoming muon polarisation
    ...
    END FUNCTION

Additionally to the particle mapping, we see that neutrinos are averaged over as indicated by :math:`\big[\bar\nu_\mu\nu_e\big]`.
We can further tell that the first initial state particle is polarised since the P-column lists a 1.


.. pid_table:: mcmule/

    m2enn0
    m2ennF
    m2ennFF
    m2ennNF

    m2ennR
    m2ennRF
    m2enng0
    m2enngV
    m2enngC

    m2ennRR
    m2enngR
    ------------------------
    m2ennee0
    m2enneeV
    m2enneeC
    m2enneeA
    m2enneeR

    t2mnnee0
    t2mnneeV
    t2mnneeC
    t2mnneeA
    t2mnneeR
    ------------------------
    m2ej0
    m2ejF
    m2ejR
    m2ejg0
    ------------------------
    em2em0
    em2emV
    em2emC
    em2emFEE
    em2emFEM
    em2emFMM
    em2emA
    em2emFFEEEE
    em2emFFMMMM
    em2emFFMIXDz
    em2emAA
    em2emAFEE
    em2emAFEM
    em2emAFMM
    em2emNFEE
    em2emNFEM
    em2emNFMM
    em2emREE
    em2emREM
    em2emRMM
    em2emRFEEEE
    em2emRFMIXD
    em2emRFMMMM
    em2emAREE
    em2emAREM
    em2emARMM
    em2emRREEEE
    em2emRRMIXD
    em2emRRMMMM

    emZem0X
    emZemFX
    emZemRX
    ------------------------
    mp2mp0
    mp2mpF
    mp2mpA
    mp2mpFF
    mp2mpAA
    mp2mpAF
    mp2mpNF
    mp2mpR
    mp2mpRF
    mp2mpAR
    mp2mpRR
    ------------------------
    ee2mm0
    ee2mmF
    ee2mmFFEEEE
    ee2mmR
    ee2mmRFEEEE
    ee2mmRREEEE

    ee2mmA
    ee2mmAA
    ee2mmNFEE
    ee2mmAFEE
    ee2mmAREE
    ------------------------
    eeZmm0
    eeZmm0X
    eeZmmFX
    eeZmmAX
    eeZmmRX
    ------------------------
    ee2ee0
    ee2eeA
    ee2eeF
    ee2eeFF
    ee2eeAA
    ee2eeAF
    ee2eeNF

    ee2eeR
    ee2eeRF
    ee2eeAR

    ee2eeRR
    ------------------------
    eb2eb0
    eb2ebF
    eb2ebFF

    eb2ebR
    eb2ebRF

    eb2ebRR
    ------------------------
    ee2nn0
    ee2nnF
    ee2nnS
    ee2nnSS
    ee2nnCC

    ee2nnR
    ee2nnRF

    ee2nnRR
