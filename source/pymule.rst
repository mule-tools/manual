.. _sec_pymule:

pymule user guide
======================

This section describes all public functions and classes in pymule.

Working with files
------------------

.. automodule:: pymule
    :noindex:
    :members: Chi2, FileRecord, setup, sigma, mergefks

Working with errors
-------------------

.. automodule:: pymule
    :noindex:
    :members:
            mergenumbers,
            plusnumbers,
            dividenumbers,
            timesnumbers,
            mergeplots,
            addplots,
            divideplots,
            scaleplot,
            integratehistogram,
            mergebins,
            printnumber,
            chisq,

Plotting
--------

.. automodule:: pymule
    :noindex:
    :members:
        mergefkswithplot, xiresidue,
        errorband, kplot, watermark,
        mulify,
        colours
