.. _sec_getting_started:

Getting started
===============

McMule is written in Fortran 95 with helper and analysis tools written in ``python``.
This guide will help you to get started using McMule by describing in detail how to calculate the NLO corrections to :math:`\tau\to[\nu\bar\nu] e\gamma`.
Since the neutrinos are not detected, we average over them, indicated by the brackets.
Hence, we have to be fully inclusive w.r.t. the neutrinos.
Still, the code allows to make any cut on the other final-state particles.
As we will see, the :term:`BR` for this process, as measured by BaBar :cite:`Lees:2015gea, Oberhof:2015snl` has a discrepancy of more than :math:`3\,\sigma` from the :term:`SM` value.
This will illustrate the importance of fully differential :term:`NLO` corrections in QED.

Obtaining the code
------------------

McMule is distributed multiple ways

 * as a precompiled executable for recent-ish GNU Linux distributions.
   To be precise, your version of glibc needs to be newer than 2.17.
   The currently supported versions of most popular distributions (CentOS, Debian, Ubuntu, Fedora, RHEL) should be fine.

 * as a Docker :term:`image` that can be used on any platform.

 * as the source code on `Gitlab <https://gitlab.com/mule-tools/mcmule>`_ that can be compiled by the user.
   This contains the current release in the default ``release`` branch as well as the developer preview (``devel``).

Here we will focus on the first method as it is by far the easiest.
For developers and tinkerers, we refer to Section :ref:`sec_compilation` on how to compile the code yourself.

First, obtain the McMule distribution from `our website <https://mule-tools.gitlab.io/download.html>`_ and extract the tarball

.. code:: bash

    $ tar xzvf mcmule-release.tar.gz
    mcmule-release/mcmule
    mcmule-release/mcmule.mod

That's it.
You can, if you want, install McMule to use it from any directory with the following commands but this is not required

.. code:: bash

   $ cp mcmule-release/mcmule /usr/local/bin
   $ cp mcmule-release/mcmule.mod /usr/local/include


To make use of McMule results, we also require the pymule python package
It can be used from the ``tools/`` folder of the McMule repository but it is recommended that the user installs it

.. code:: bash

   $ pip3 install git+https://gitlab.com/mule-tools/pymule.git


.. _sec_getting_started_lo:

Simple runs at LO
-----------------

Setting McMule up
^^^^^^^^^^^^^^^^^
In this example we want to compute two distributions, the invariant mass of the :math:`e\gamma` pair, :math:`m_{\gamma e}\equiv \sqrt{(p_e+p_\gamma)^2}`, and the energy of the electron, :math:`E_e`, in the rest frame of the tau.
To avoid an :term:`IR` singularity in the :term:`BR`, we have to require a minimum energy of the photon.
We choose this to be :math:`E_\gamma \ge 10\,{\rm MeV}` as used in :cite:`Lees:2015gea, Oberhof:2015snl`.

At first, we need to find out how the process :math:`\tau\to\nu\bar\nu e\gamma` is implemented in McMule.
For this, we refer to the table in Section :ref:`sec_pieces` that specifies the pieces (sometimes called :f:var:`which_piece`) that is required for a :term:`generic processes`.
The generic process is a prototype for the physical process such as :math:`\ell \to\nu\bar\nu \ell' \gamma` where the flavour of the lepton :math:`\ell` is left open.
In our case, we need to consider the row for :math:`\mu\to\nu\bar\nu e\gamma`.
Since we are only interested in :term:`LO`, the only :f:var:`which_piece` we need is ``m2enng0``.
To change from the generic process :math:`\mu\to\nu\bar\nu e\gamma` to the process we are actually interested in, :math:`\tau\to\nu\bar\nu e\gamma`, we pick the :f:var:`flavour` ``tau-e`` which refers to a :math:`\tau \to e \cdots` transition.
Other options would be ``tau-mu`` for :math:`\tau\to\mu \cdots`` or ``mu-e`` for :math:`\mu\to e\cdots`.

Next, we need to find out which particle ordering is used in McMule for this piece, i.e. which variable will contain eg. the electron momentum.
This is called the particle identification or :term:`PID`.
We can refer to the table in Section :ref:`sec_pid` to find that for the :f:var:`which_piece` ``m2enng0``, we have

.. math::

   \mu^-(p_1) \to e^-(p_2) \big[\bar\nu_e\nu_\mu](p_3,p_4) \gamma(p_5)

We can now implement our observables.
For this, we need to define a ``user.f95`` file in the ``src`` folder.
An empty template can be found in `the file tools/user-empty.f95 <https://gitlab.com/mule-tools/mcmule/-/blob/master/tools/user-empty.f95>`_.
We can use this file to the :term:`measurement function` we want to calculate, i.e. which distributions and cuts we want to apply.
We can further add some code that will execute at the beginning of the Monte Carlo run (allowing us eg. to further configure our calculation) and for each event (to simulate beam spread).

We begin by specifying the metadata of our histograms: we want two histograms (``nr_q = 2``) with 90 bins each (``nr_bins = 90``).
The ranges should be :math:`0 < m_{\gamma e} < 1800\,{\rm MeV}` and :math:`0 \le E_e \le 900\,{\rm MeV}`.

.. literalinclude:: mcmule/example1/user.f90
   :language: fortran
   :lineno-start: 12
   :lines: 12-17
   :caption: The metadata for our calculation with two histograms (``nr_q = 2``) with 90 bins each (``nr_bins = 90``). The ranges should be :math:`0 < m_{\gamma e} < 1800\,{\rm MeV}` and :math:`0 \le E_e \le 900\,{\rm MeV}`.

.. note::
   Finding suitable values for the ranges can be tricky beyond :term:`LO` and usually requires a few test runs.
   Since all histograms have the same number of bins, one is often forced to have empty bins to ensure 'nice' bin widths.

We can now define the actual :term:`measurement function` called :f:func:`quant`.
We need to

 * calculate the invariant mass of the :math:`e\gamma` pair.
   This is done using the momentum-squaring function :f:func:`sq`.
   The result is store in the first distribution, ``quant(1)``.

 * store the electron energy in ``quant(2)``.
   Since this is frame-dependent, we need to know that McMule generates the particles in the tau rest frame.
   However, in general it is better to boost into that frame.
   Further, McMule stores momenta as ``(/px,py,pz,E/)``, meaning the energy is ``q2(4)``.

 * cut on the photon energy ``q5(4)``.
   The variable :f:var:`pass_cut` controls the cuts. Initially it is set to ``.true.``, to indicate that the event is kept.
   Applying a cut amounts to setting pass_cut to ``.false.``.

.. literalinclude:: mcmule/example1/user.f90
   :language: fortran
   :lineno-start: 62
   :lines: 62-78
   :caption: The :term:`measurement function` at :term:`LO`

Additionally to the numeric value in ``quant(i)`` we store a human-readable name in ``names(i)``.

.. warning::
    The maximal length of these names is defined in the variable ``namesLen`` which defaults to 6 characters.

    Also note that this :term:`measurement function` is not :term:`IR`-safe!


We now need to compile our observable into a :term:`shared library` so that McMule can load it.
To do this, we run

.. code:: bash

   $ gfortran -fPIC --shared -o user.so user.f95

This requires the ``mcmule.mod`` file to either be in the current directory or installed somewhere the compiler can find it.
Otherwise, one needs to add the corresponding flag

.. code:: bash

   $ gfortran -I/path/to/the/folder/of/mcmule.mod/ -fPIC --shared -o user.so user.f95

We now need to re-compile McMule to ensure that we have the correct version of ``user.f95``.

.. warning::
    The ``mcmule.mod`` header file is autogenerated by GFortran during the compilation of McMule.
    If you are using a copy of GFortran prior to version 8, this means you will have to regenerate the header file manually.
    To do this, you can use the ``build-header.sh`` script.

Running McMule manually
^^^^^^^^^^^^^^^^^^^^^^^

Now the mule is ready to trot.
For quick and dirty runs of McMule, the easiest way is to just execute the ``mcmule`` binary in the same directory as the ``user.so`` file and input the configuration by hand.
However, since this is not how the code is meant to be used, it will not prompt the user but just expect the correct input.

We now need to choose the statistics we want. For this example, we pick 10 iterations with :math:`1000\times 10^3` points each for pre-conditioning and 50 iterations with :math:`1000\times 10^3` points each for the actual numerical evaluation (cf. Section :ref:`sec_stat` for some heuristics to determine the statistics needed).
We pick a :term:`random seed` between 0 and :math:`2^{31}-1` (cf. Section :ref:`sec_rng`), say 70998, and for the input variable :f:var:`which_piece` we enter ``m2enng0`` as discussed above.
The :f:var:`flavour` variable is now set to ``tau-e`` to change from the generic process :math:`\mu\to\nu\bar\nu e\gamma` to the process we are actually interested in, :math:`\tau\to\nu\bar\nu e\gamma`.
This system is used for other processes as well.
The input variable :f:var:`which_piece` determines the generic process and the part of it that is to be computed (i.e. tree level, real, double virtual etc.).
In a second step, the input :f:var:`flavour` associates actual numbers to the parameters entering the matrix elements and phase-space generation.
This means that we need to input the following (the specifications for the input can be found in :numref:`tab_mcmuleinput`):

.. warning::

   When running ``mcmule`` outside the normal repository, you need to make sure that an ``out/`` folder exists.

.. code-block::

    $ gfortran -fPIC --shared -o user.so user.f95
    $ ./mcmule
    1000
    10
    10000
    50
    70998
    1.0
    1.0
    m2enng0
    tau-e


             *******************************************
             *              C O L L I E R              *
             *                                         *
             *        Complex One-Loop Library         *
             *      In Extended Regularizations        *
             *                                         *
             *    by A.Denner, S.Dittmaier, L.Hofer    *
             *                                         *
             *              version 1.2.3              *
             *                                         *
             *******************************************

       - * - * - * - * - * - * - * -
            Version information
      Full SHA: 3342511
      Git  SHA: 1fbc291
      Git branch: HEAD
       - * - * - * - * - * - * - * -
     Calculating tau->e nu nu gamma at LO
       - * - * - * - * - * - * - * -

     internal avgi, sd:    31902651645147.434        3242845143300.6875
     internal avgi, sd:    36962119569527.797        1491060763146.8340
     internal avgi, sd:    39908483081760.562        701506532475.22485
     internal avgi, sd:    41908326436302.352        183707731215.21738
     internal avgi, sd:    41771416194096.336        55441877946.459091
     internal avgi, sd:    41871492562379.680        27645368422.638184
     internal avgi, sd:    41870973597620.547        21172712863.774796
     internal avgi, sd:    41881968277900.094        17287639806.820400
     internal avgi, sd:    41894819976244.469        15148087824.181145
     internal avgi, sd:    41892443511666.180        13860145189.905710
     internal avgi, sd:    41883909931737.320        9081654737.2369480
     internal avgi, sd:    41891877400107.203        5996399688.5281315
     internal avgi, sd:    41887401454137.172        4967120009.5028763
     internal avgi, sd:    41894988589984.109        4318086453.2893734
     internal avgi, sd:    41895218930734.938        3855189670.2831044
     internal avgi, sd:    41893628691682.039        3569029881.5161963
     internal avgi, sd:    41895702521658.094        3301046354.0162683
     internal avgi, sd:    41894921420510.164        3068605199.3146548
     internal avgi, sd:    41894380982483.836        2884341089.4262500
     internal avgi, sd:    41894136940077.953        2719744511.4164872
     internal avgi, sd:    41894755045877.328        2575585554.2240872
     internal avgi, sd:    41894180414331.000        2448105950.1048141
     internal avgi, sd:    41892974586371.242        2335940686.3421283
     internal avgi, sd:    41892018243977.422        2237743190.2910728
     internal avgi, sd:    41892128399199.852        2151548541.7080536
     internal avgi, sd:    41891054946079.172        2079987935.2725692
     internal avgi, sd:    41890529496649.336        2009220806.5744867
     internal avgi, sd:    41889627128683.867        1952022430.9618585
     internal avgi, sd:    41889091169697.750        1901209181.0766854
     internal avgi, sd:    41889491086513.711        1851335964.4142988
     internal avgi, sd:    41889024177143.492        1804584253.3775585
     internal avgi, sd:    41888652800094.414        1763879105.2402925
     internal avgi, sd:    41888186242209.695        1734933321.8742726
     internal avgi, sd:    41888838662647.031        1702558920.3787835
     internal avgi, sd:    41888878166048.805        1664687915.8245957
     internal avgi, sd:    41888871786161.102        1628412032.4284451
     internal avgi, sd:    41888673286317.961        1598222188.0324447
     internal avgi, sd:    41888363240043.000        1570566301.1038322
     internal avgi, sd:    41888533287695.047        1541834130.0455377
     internal avgi, sd:    41888087919550.688        1514438031.2038479
     internal avgi, sd:    41887838382975.297        1487390337.2575607
     internal avgi, sd:    41887692329889.953        1465777595.5131192
     internal avgi, sd:    41887528786746.531        1450928637.2665195
     internal avgi, sd:    41887814931451.086        1432276674.4390638
     internal avgi, sd:    41887764015763.508        1409275835.0397925
     internal avgi, sd:    41887871329949.469        1388512123.7455208
     internal avgi, sd:    41887728279057.234        1369450030.9152539
     internal avgi, sd:    41887673022843.117        1350554230.4978600
     internal avgi, sd:    41887824787223.562        1332418851.3856776
     internal avgi, sd:    41887720562604.266        1315515744.3643637
     internal avgi, sd:    41887747627717.641        1297906527.3172038
     internal avgi, sd:    41887385610296.359        1280408319.1259799
     internal avgi, sd:    41887163475026.672        1262887224.9655898
     internal avgi, sd:    41887020587065.422        1248392301.3985555
     internal avgi, sd:    41886965905979.375        1236043197.7830524
     internal avgi, sd:    41887132288349.984        1225259563.1290646
     internal avgi, sd:    41887118281531.000        1211732414.0191844
     internal avgi, sd:    41887256099447.883        1199948076.9753296
     internal avgi, sd:    41887425753145.656        1188759649.9116085
     internal avgi, sd:    41887079359692.539        1176252324.5589268
     points:    10*  1M +  50* 10M         random seed:      70998
     part: m2enng0                      xicut: 1.00000    delcut: 1.00000
     points on phase space  321225751 thereof fucked up          0



     result, error:  {  4.18871E+13,  1.17625E+09 };   chisq:  1.27

       - * - * - * - * - * - * - * -

McMule begins by printing some auto-versioning information (the :term:`SHA1` hashes of the source code and the git version) as well as some user-defined information from the subroutine :f:subr:`inituser`.
Next, the integration begins.
After every iteration, McMule prints the current best estimate and error of the total cross section or decay rate.
Before exiting, it will also print again the input used as well as the number of points evaluated and the final result.
This run took approximately 15 minutes.

.. _tab_mcmuleinput:
.. table:: The options read from ``stdin`` by McMule. The calls are multiplied by 1000.
   :widths: auto

   ====================== ============== =================================================================================
   **Variable name**      **Data type**  **Comment**
   ====================== ============== =================================================================================
   ``nenter_ad``          ``integer``    calls / iteration during pre-conditioning
   ``itmx_ad``            ``integer``    iterations during pre-conditioning
   ``nenter``             ``integer``    calls / iteration during main run
   ``itmx``               ``integer``    iterations during main run
   ``ran_seed``           ``integer``    :term:`random seed` :math:`z_1`
   ``xinormcut``          ``real(prec)`` the :math:`0<\xi_{c}\le1` parameter
   ``delcut``             ``real(prec)`` the :math:`\delta_{\text{cut}}` parameter (or at :term:`NNLO` the second :math:`\xi_{c}`)
   :f:var:`which_piece`   ``char(10)``   the part of the calculation to perform
   :f:var:`flavour`       ``char(8)``    the particles involved
   (opt)                  unknown        the user can request further input during :f:subr:`inituser`
   ====================== ============== =================================================================================


Analysing the output
^^^^^^^^^^^^^^^^^^^^
After running McMule we want to calculate the actual cross section or decay rate and make plots.
The McMule output is saved to the ``out/`` folder as a ``.vegas`` file that contains the entire state of the integrator (cf. Section :ref:`sec_vegasff`).
We can open this file in python and make plots.

While it is possible to open just a single file using  :func:`~pymule.importvegas`, this is rarely done as real-world calculations can involve hundreds of ``.vegas`` files.
Instead, we move the ``.vegas`` file into a new directory, say ``example1`` and then use :func:`~pymule.sigma` and :func:`~pymule.mergefks`.

.. literalinclude:: mcmule/dummy.py
   :language: python
   :linenos:
   :lines: 1-12

.. warning::
   In McMule the numerical value of the Fermi constant :math:`G_F` and the fine-structure constant :math:`\alpha` are set to one for predominately historical reasons.
   This needs to be restored in python, eg. using :func:`~pymule.scaleset`

The variable ``dat`` now contains the runtime (``time``), branching ratio (after multiplication with the lifetime, ``value``), and :math:`\chi^2` of the integration (``chi2a``) as well as our distributions (``Ee`` and ``minv``).
Numerical values such as cross sections or branching ratios are stored as numpy arrays with errors as ``np.array([y, dy])``.
Distributions are stored as numpy :math:`N\times 3` matrices

.. code:: python

    np.array([[x1, y1, dy1],
              [x2, y2, dy2],
              [x3, y3, dy3],
              [x4, y4, dy4],
              [x5, y5, dy5],
              ...
              [xn, yn, dyn]])

These can be manipulated eg. using the tools of pymule described in Section :ref:`sec_pymule`.
For now, we will just make a plot of the :math:`E_e` distribution ``Ee``

.. literalinclude:: mcmule/dummy.py
   :language: python
   :lineno-start: 14
   :lines: 14-20

.. figure:: mcmule/dummy.svg

   Result of the :term:`LO` test run for the :math:`E_e` distribution

.. _sec_getting_started_nlo:

Running at NLO and beyond
-------------------------

A few things change once we go beyond :term:`LO` since we can have extra radiation.
To account for this, more :f:var:`which_piece` need to be ran and then correctly combined.
This also increases the number of runs necessary, meaning that the manual approach from above is no longer feasible.

Setting McMule up
^^^^^^^^^^^^^^^^^
Referring back to Section :ref:`sec_pieces` we find that we need the pieces ``m2enngF`` and ``m2enngR`` for virtual and real corrections respectively.
The :term:`PID` table of Section :ref:`sec_pid` tells us that the real photon can is going to be ``q6``.

We first need to decide whether we want to calculate exclusive or inclusive decays.
The details here depend on the exact experimental situation which can be tricky to properly implement.
Following the BaBar analysis :cite:`Lees:2015gea, Oberhof:2015snl` we will consider the exclusive radiative decay, i.e. we request precisely one photon with energy :math:`E_\gamma>10\,{\rm MeV}`.
The function :f:func:`quant` will have to take this into account with the additional argument ``q6``, the momentum of the second photon.

To ensure :term:`IR` safety, we define the harder and softer photon ``gh`` and ``gs``, respectively, and require that the former (latter) has energy larger (smaller) than :math:`10\,{\rm MeV}`.
This new version of :f:func:`quant` is also suitable for the :term:`LO` calculation and it is generally advisable to use a single :f:func:`quant` function for all parts of a computation.

.. literalinclude:: mcmule/example2/user.f90
   :language: fortran
   :lineno-start: 62
   :lines: 62-85
   :caption: The measurement function beyond :term:`LO`. The changes w.r.t. to :term:`LO` are highlighted.
   :emphasize-lines: 5,9-16

Running McMule
^^^^^^^^^^^^^^

The :term:`FKS` scheme used in McMule introduces an unphysical parameter called :math:`\xi_c` that can be varied between

.. math::

   0<\xi_{c}\le\xi_{\text{max}} = 1-\frac{\big(\sum_i m_i\big)^2}{s}

Checking the independence of physical results on the latter serves as a consistency check, both of the implementation of McMule but also of the :term:`IR` safety of the :term:`measurement function`.
To do this, it can help to disentangle ``m2enngF`` into ``m2enngV`` and ``m2enngC`` though this is not necessary.
Only the latter depends on :math:`\xi_c` and this part is typically much faster in the numerical evaluation.

A particularly convenient way to run McMule is using :term:`menu files<menu file>` [1]_.
A :term:`menu file` contains a list of jobs to be computed s.t. the user will only have to vary the :term:`random seed` and :math:`\xi_{c}` by hand as the statistical requirements are defined globally in a :term:`config file`.
This is completed by a :term:`submission script`, usually called ``submit.sh``.
The submit script is what will need to be launched which in turn will take care of the starting of different jobs.
It can be run on a normal computer or on a SLURM cluster :cite:`Yoo:2003slurm`.
To prepare the run in this way we can use pymule

.. literalinclude:: listings/pymule-int.txt
   :caption: The steps necessary to use pymule to prepare running McMule. Note that numbers listed as seeds are random and hence not reproducible.
   :language: bash
   :name: lst_pymulein

When using the tool, we are asked various questions, most of which have a default answer in square brackets.
In the end pymule will create a directory that the user decided to call ``example2``, where all results will be stored.
The :term:`menu<menu file>` and :term:`config<config file>` files generated by pymule are shown in :numref:`lst_menu` and :numref:`lst_conf`

.. literalinclude:: mcmule/example2/menu-m2enng-tau-e.menu
   :caption: :term:`menu file` for the present calculation
   :language: bash
   :name: lst_menu

.. literalinclude:: mcmule/example2/m2enng-tau-e.conf
   :caption: Configuration file for the present calculation
   :language: bash
   :name: lst_conf

To start ``mcmule``, we now just need to execute the created ``example2/submit.sh`` after copying the user library ``user.so`` into the same folder.
Note that per default this will spawn at most as many jobs as the computer pymule ran on had CPU cores.
If the user wishes a different number of parallel jobs, change the fifth line of ``example2/submit.sh`` to

.. code:: bash

   #SBATCH --ntasks=<number of cores>

To now run McMule, just execute

.. code:: bash

   $ nohup ./example2/submit.sh &

The ``nohup`` is not technically necessary but advisable, especially on remote systems.
When running on a SLURM system, the other SLURM parameters ``--partition``, ``--time``, and ``--clusters`` need to be adapted as well.

.. warning::
   The :term:`submission script` will call itself on multiple occasions.
   Therefore, it is not advisable to change its name or the name of the run directory without taking precautions.


.. _sec_analyse:

Analysing the results
^^^^^^^^^^^^^^^^^^^^^

After running the code, we need to combine the various :f:var:`which_piece` into physical results that we will want to use to create plots.
This is the moment where pymule's :func:`~pymule.mergefks` shines.

.. literalinclude:: mcmule/babar.py
   :caption: An example code to analyse the results for :math:`\tau\to\nu\bar\nu e\gamma` in pymule. Note that, in the Fortran code :math:`G_F = \alpha = 1`. In pymule they are at their physical values.
   :language: python
   :name: lst_pymule
   :lines: 8-49

Once pymule is imported and setup, we import the :term:`LO` and :term:`NLO` :f:var:`which_piece` and combine them using two central pymule commands that we have encountered above: :func:`~pymule.sigma` and :func:`~pymule.mergefks`.
:func:`~pymule.sigma` takes the :f:var:`which_piece` as an argument and imports matching results, already merging different :term:`random seeds<random seed>`.
:func:`~pymule`mergefks` takes the results of (multiple) :func:`~pymule.sigma` invocations, adds results with matching :math:`\xi_{c}` values and combines the result.
In the present case, :math:`\sigma_n^{(1)}` is split into multiple contributions, namely ``m2enngV`` and ``m2enngC``.
This is indicated by the ``anyxi`` argument.

Next, we can use some of pymule\ ’s tools (cf.  Listing :numref:`lst_pymule`) to calculate the full :term:`NLO` :term:`BR`\ s from the corrections and the :term:`LO` results

.. math::
   \mathcal{B}|_{\text{LO}} &= 1.8339(1) \times 10^{-2}\\
   \mathcal{B}|_{\text{NLO}} &= 1.6451(1) \times 10^{-2}

which agree with :cite:`Fael:2015gua,Pruna:2017upz`, but :math:`\mathcal{B}|_{\text{NLO}}` is in tension with the value :math:`\mathcal{B}|_{\text{exp}} = 1.847(54)\times 10^{-2}` reported by BaBar :cite:`Lees:2015gea, Oberhof:2015snl`.
As discussed in :cite:`Pruna:2017upz, Ulrich:2017adq` it is very likely that this tension would be removed if a full :term:`NLO` result was used to take into account the effects of the stringent experimental cuts to extract the signal.
This issue has been explained in detail in :cite:`Pruna:2017upz, Ulrich:2017adq, Ulrich:2020phd`.

As a last step, we can use the ``matplotlib``-backed :func:`~pymule.kplot` command to present the results for the distributions (logarithmic for :math:`m_{e\gamma}` and linear for :math:`E_e`).
The upper panel of :numref:`fig_babares_minv` shows the results for the invariant mass :math:`m_{e\gamma}` at :term:`LO` (green) and :term:`NLO` (blue) in the range :math:`0\le m_{e\gamma} \le 1\,{\rm GeV}`.
Note that this, for the purposes of the demonstration, does not correspond to the boundaries given in the run.

.. _fig_babares_minv:
.. figure:: mcmule/tau:minv.svg

   Results of the toy run to compute :math:`m_{e\gamma}` for :math:`\tau\to\nu\bar\nu e\gamma`. Upper panels show the :term:`LO` (green) and :term:`NLO` (blue) results, the lower panels show the :term:`NLO` K factor.

The distribution falls sharply for large :math:`m_{e\gamma}`.
Consequently, there are only few events generated in the tail and the statistical error becomes large.
This can be seen clearly in the lower panel, where the :term:`NLO` :math:`K` factor is shown.
It is defined as

.. math::
       K^{(1)} = 1+
            \frac{\mathrm{d}\sigma^{(1)}}
                 {\mathrm{d}\sigma^{(0)}}

and the band represents the statistical error of the Monte Carlo integration.
To obtain a reliable prediction for larger values of :math:`m_{e\gamma}`, i.e. the tail of the distribution, we would have to perform tailored runs.
To this end, we should introduce a cut :math:`m_{\text{cut}}\ll m_\tau` on :math:`m_{e\gamma}` to eliminate events with larger invariant mass.
Due to the adaption in the numerical integration, we then obtain reliable and precise results for values of :math:`m_{e\gamma} \lesssim m_{\text{cut}}`.

:numref:`fig_babares_en` shows the electron energy distribution, again at :term:`LO` (green) and :term:`NLO` (blue).
As for :math:`m_{e\gamma}` the corrections are negative and amount to roughly :math:`10\%`.
Since this plot is linear, they can be clearly seen by comparing :term:`LO` and :term:`NLO`.
In the lower panel once more the :math:`K` factor is depicted.
Unsurprisingly, at the very end of the distribution, :math:`E_e\sim 900\,{\rm MeV}`, the statistics is out of control.

.. _fig_babares_en:
.. figure:: mcmule/tau:energy.svg

   Results of the toy run to compute :math:`E_e` for :math:`\tau\to\nu\bar\nu e\gamma`. Upper panels show the :term:`LO` (green) and :term:`NLO` (blue) results, the lower panels show the :term:`NLO` K factor.

More complicated runs
---------------------

To demonstrate some of McMule capabilities, we tweak the observable a bit.
Since the tau is usually produced in :math:`e^+e^-\to \tau^+\tau^-`, we instead use the LO cross section

.. math::
   :label: eq_costh

    \frac{{\rm d}\sigma}{{\rm d}(\cos\theta)} \propto
    \Big(1+\frac{4m_\tau^2}{s}\Big)
    + \Big(1+\frac{4m_\tau^2}{s}\Big) \cos\theta

with :math:`\sqrt s = m_{\Upsilon(4S)} = 10.58\,{\rm GeV}`.

To accurately simulate this situation, we need to

 * choose a random value for :math:`\theta`,

 * construct the tau momentum :math:`p_1` in the lab frame,

 * boost the momenta from McMule into this frame, and

 * apply a correction weight from :eq:`eq_costh`

for every event.
We require the following cuts in the lab frame

 * the produced electron and hard photon have :math:`-0.75 \le \cos\theta_{i,e^-} \le +0.95`

 * the hard photon energy is bigger than 220 MeV

Further, we want to have a switch for inclusive and exclusive measurements without having to adapt the user file.

Asking for user input
^^^^^^^^^^^^^^^^^^^^^
To be able to switch cuts on and off, we need to read input from the user at runtime.
This can be done in the :f:subr:`inituser` routine where input can be ``read``.
We can store the result in a global variable (``exclusiveQ``) so we can later use it in :f:func:`quant`.
Further, we need modify the name of the vegas file by changing :f:var:`filenamesuffix`.
It is also good practice to ``print`` the configuration chosen for documentation purposes.

.. literalinclude:: mcmule/example3/user.f95
   :language: fortran
   :lineno-start: 57
   :lines: 57-70

.. note::

    When using the :term:`menu file` system, this can only be a single integer.
    To read multiple bits of information, you need to encode the data somehow.


Generation of the tau momentum
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We can use the user integration feature of McMule to generate :math:`\cos\theta`.
This allows us to write

.. math::

   \sigma \sim \int_0^1 {\rm d} x_1 \int_0^1 {\rm d} x_2 \cdots \int_0^1 {\rm d} x_m
        \times \int {\rm d}\Phi\,\,
        \left\vert\mathcal{M}_n\right\vert^2\,\,
        f(x_1,x_2,\cdots,x_n;p_1,\cdots,p_n)

with a generalised :term:`measurement function` :math:`f`.
Since :eq:`eq_costh` is sufficiently simple, we will sample  :math:`\cos\theta` with a uniform distribution and apply a correction weight rather trying to sample it directly.
We set the variable :f:var:`userdim` to one to indicate that we want to carry out :math:`m=1` extra integrations and define the function :f:subr:`userevent` that sets global variable ``cth`` for :math:`\cos\theta`

.. literalinclude:: mcmule/example3/user.f95
   :language: fortran
   :lineno-start: 133
   :lines: 133-139

.. warning::
   This function can be used to change the centre-of-mass energy and masses of the particles.
   However, one must the re-compute the flux factors and :math:`\xi_{\text{max}}` relations.

Boosting into the lab frame
^^^^^^^^^^^^^^^^^^^^^^^^^^^

We begin by writing down the momentum of tau in the lab frame as

.. math::
   p_1 = \Big( 0, |\vec p| \sqrt{1-\cos\theta^2}, |\vec p| \cos\theta, E\Big)

with :math:`|\vec p| = \sqrt{E^2-m_\tau^2}`.
Next, we use the McMule function :f:func:`boost_back` to boost the momenta we are given into the lab frame.
From there we can continue applying our cuts as before, utilising the McMule function :f:var:`cos_th` to calculate the angle between the particle and the beam axis.

.. literalinclude:: mcmule/example3/user.f95
   :language: fortran
   :lineno-start: 73
   :lines: 73-130

Running and analysis
^^^^^^^^^^^^^^^^^^^^

At this point we can run McMule and proceed with the analysis as before.
We need to do two runs, one for the exclusive and one for the inclusive.
However, only the real corrections differ, therefore we only need 24 runs and not 36.
The last argument of the ``run`` command in the :term:`menu file` will be passed as the observable we have defined in :f:subr:`inituser`.
We need to pass ``1`` (exclusive) or ``0`` (inclusive) as shown in :numref:`lst_menuv3` [2]_

.. literalinclude:: mcmule/example3/menu-m2enng-tau-e.menu
   :caption: The :term:`menu file` for the present calculation
   :language: bash
   :name: lst_menuv3

We can now run McMule.
When analysing the output we need to take care to not mix the different observables which we do by passing the optional argument ``obs`` to :func:`~pymule.sigma`.
The resulting plot is shown in :numref:fig_Eboost:

.. literalinclude:: mcmule/babar.py
   :caption: The analysis pipeline for this calculation
   :language: python
   :lines: 62-101

.. _fig_Eboost:
.. figure:: mcmule/tau:boost.svg

   Results of the toy run to compute :math:`E_{e}` in the labframe.

.. [1]
   The name menu was originally used by the cryptanalysts at Bletchley Park to describe a particular set of configurations for the 'computer' to try

.. [2]
   Looking at the results from our previous run, we can deduce that :math:`\xi_c\sim0.7` is the optimal place for running since :math:`\sigma_n(\xi_c=0.7)\sim \sigma_{n+1}(\xi_c=0.7)` which reduces cancellation between the different pieces.
   This optimisation is not strictly necessary and we still run for two values of :math:`\xi_c`, 0.6 and 0.8.
