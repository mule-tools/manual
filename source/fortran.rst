.. _sec_fortran:

Fortran reference guide
=======================

McMule's Fortran code has hundreds of functions and subroutine and we will not document all of them here.
However, we will list the user-facing function that are intended to help construct user files.


User-modifiable parameters
--------------------------

The following parameters may be modified by the user though it might become necessary to completely recompile McMule ones done.

.. f:currentmodule:: global_def

.. f:type:: real(kind=prec)
   :attrs: fixed

   The real number type used in McMule.
   This cannot be changed at runtime by the user but should be used for all interactions with the code.
   It usually refers to double precision

.. f:variable:: real(kind=prec) musq

   The renormalisation scale :math:`\mu^2`.
   This variable *needs* to be set by the user, otherwise McMule will fail.

.. f:variable:: integer nel

   Set to 1 if electron :term:`VP` loops are to be included, set to 0 otherwise.
   More options may be added later

.. f:variable:: integer nmu

   Set to 1 if muon :term:`VP` loops are to be included, set to 0 otherwise.
   More options may be added later

.. f:variable:: integer ntau

   Set to 1 if tau :term:`VP` loops are to be included, set to 0 otherwise.
   More options may be added later

.. f:variable:: integer nhad

   Set to 1 if :term:`HVP` loops are to be included, set to 0 otherwise.
   More options may be added later

.. f:variable:: real(kind=prec) pol1(4)

   The polarisation of the first polarised particle

.. f:variable:: real(kind=prec) pol2(4)

   The polarisation of the second polarised particle

.. f:function:: real(kind=prec) sachs_gel(q2)

   The electric Sachs form factor of the proton.
   In the dipole approximation this is

   .. math::

      G_e(Q^2) = \frac1{(1+Q^2/\Lambda)^2}

   :p real(kind=prec) q2: the value of :math:`Q^2`

.. f:function:: real(kind=prec) sachs_gmag(q2)

   The magnetic Sachs form factor of the proton.
   In the dipole approximation this is

   .. math::

      G_m(Q^2) = \frac{\kappa}{(1+Q^2/\Lambda)^2}

   :p real(kind=prec) q2: the value of :math:`Q^2`

.. f:subroutine:: init_flavour(flavour)

   The definitions of the :f:var:`flavour`.
   Users may edit this to add new experiments etc.

.. f:variable:: real(kind=prec) GF
   :attrs: fixed

   The Fermi constant.
   For predominantly historic reasons, this is set to ``1._prec``.

.. f:variable:: real(kind=prec) alpha
   :attrs: fixed

   The fine-structure constant in the :term:`OS` scheme.
   For predominantly historic reasons, this is set to ``1._prec``.

.. f:variable:: real(kind=prec) sw2

   The weak mixing angle :math:`\sin(\theta_W)^2`.
   This can be changed by the user at runtime to modify the :term:`EW` scheme that is used.

.. f:variable:: real(kind=prec) Mel
   :attrs: fixed

   The numerical value of the electron mass in MeV, irregardless of the :f:var:`flavour`

.. f:variable:: real(kind=prec) Mmu
   :attrs: fixed

   The numerical value of the muon mass in MeV, irregardless of the :f:var:`flavour`

.. f:variable:: real(kind=prec) Mtau
   :attrs: fixed

   The numerical value of the tau mass in MeV, irregardless of the :f:var:`flavour`

.. f:variable:: real(kind=prec) Mproton
   :attrs: fixed

   The numerical value of the proton mass in MeV, irregardless of the :f:var:`flavour`

.. f:variable:: real(kind=prec) MZ
   :attrs: fixed

   The numerical value of the Z boson mass in MeV

.. f:variable:: real(kind=prec) Mm
   :attrs: protected

   The actual value of the ``m`` particle, usually the muon mass but if :f:var:`flavour` is eg. ``tau-e`` the tau mass

.. f:variable:: real(kind=prec) Me
   :attrs: protected

   The actual value of the ``e`` particle, usually the electron mass but if :f:var:`flavour` is eg. ``tau-mu`` the muon mass

.. f:variable:: real(kind=prec) Mt
   :attrs: protected

   The actual value of the ``t`` particle, usually the tau mass

.. f:variable:: real(kind=prec) scms
   :attrs: protected

   The numerical value of the centre-of-mass energy

.. f:variable:: real(kind=prec) lambda
   :attrs: protected

   The dipole coefficient in the Sachs form factors of the proton in MeV:sup:`2`

.. f:variable:: real(kind=prec) kappa
   :attrs: protected

   The magnetic moment of the proton in Sachs form factors

Technical parameters
--------------------

The following parameters should not be modified by the user unless especially advised to do so

.. f:variable:: character which_piece(25)
   :attrs: advanced

   The piece being integrated, cf. Section :ref:`sec_pieces`

.. f:variable:: character flavour(15)
   :attrs: advanced

   The flavour configuration being used

.. f:variable:: real(kind=prec) softcut
   :attrs: advanced

   The value of :math:`\xi` below which the integrand is set to zero without subtraction

.. f:variable:: real(kind=prec) colcut
   :attrs: advanced

   The value of :math:`\cos\theta` below which the integrand is set to zero

.. f:variable:: real(kind=prec) sSwitch
   :attrs: advanced

   The value of :math:`\xi` below which the matrix element is approximated at :term:`LP`.
   This is only available for some matrix elements

.. f:variable:: real(kind=prec) ntsSwitch
   :attrs: advanced

   The value of :math:`\xi` below which the matrix element is approximated at :term:`NLP`.
   This is only available for some matrix elements


User-facing functions
---------------------

The following function are available for the user to construct observables.
Momenta are of the form ``(/ px, py, pz, E /)``.

.. f:currentmodule:: functions

Scalar quantities
^^^^^^^^^^^^^^^^^

.. f:function:: real(kind=prec) s(p1, p2)

   The scalar product :math:`2p_1\cdot p_2`

   :p real(kind=prec) p1(4): the first momentum :math:`p_1`
   :p real(kind=prec) p2(4): the second momentum :math:`p_2`

.. f:function:: real(kind=prec) sq(p)

   The Lorentz square :math:`p^2`

   :p real(kind=prec) p(4): the momentum :math:`p`

.. f:function:: real(kind=prec) asymtensor(p1, p2, p3, p4)

   The total asymmetric tensor :math:`\varepsilon_{\mu\nu\rho\sigma} p_1^\mu p_2^\nu p_3^\rho p_4^\sigma`

   :p real(kind=prec) p1(4): the momentum :math:`p_1`
   :p real(kind=prec) p2(4): the momentum :math:`p_2`
   :p real(kind=prec) p3(4): the momentum :math:`p_3`
   :p real(kind=prec) p4(4): the momentum :math:`p_4`

.. f:function:: real(kind=prec) eta(p)

   The pseudorapidity w.r.t. the :math:`z` axis

   .. math::

      \eta = \frac12\log\frac{|\vec p| + p_z}{|\vec p| - p_z}

   :p real(kind=prec) p(4): the momentum :math:`p`

.. f:function:: real(kind=prec) rap(p)

   The rapidity w.r.t. the :math:`z` axis

   .. math::

      y = \frac12\log\frac{E + p_z}{E - p_z}

   :p real(kind=prec) p(4): the momentum :math:`p`

.. f:function:: real(kind=prec) pt(p)

   The transverse momentum w.r.t. the :math:`z` axis

   .. math::

      p_T = \sqrt{p_x^2+p_z^2}

   :p real(kind=prec) p(4): the momentum :math:`p`

.. f:function:: real(kind=prec) absvec(p)

   The length of the three-vector part :math:`|\vec p`

   :p real(kind=prec) p(4): the momentum :math:`p`

.. f:function:: real(kind=prec) phi(p)

   The azimuthal angle of :math:`p`, :math:`-\pi<\phi<\pi`

   :p real(kind=prec) p(4): the momentum :math:`p`

   .. note::

      This may return :math:`100\pi` if the calculation fails.

.. f:function:: real(kind=prec) rij(p1, p2)

   The jet distance :math:`R_{12}` between the two momenta :math:`p_1` and :math:`p_2`, normalised by :math:`D_{\text{res}} = 0.7`

   .. math::

      R_{12} = \frac{\Delta y_{12}^2 + \Delta\phi_{12}^2}{D_\text{res}^2}

   :p real(kind=prec) p1(4): the first momentum :math:`p_1`
   :p real(kind=prec) p2(4): the second momentum :math:`p_2`

.. f:function:: real(kind=prec) cos_th(p1, p2)

   The cosine of the angle between the two momenta :math:`p_1` and :math:`p_2`

   .. math::

      \cos\theta_{12} = \frac{\vec p_1\cdot \vec p_2}{|\vec p_1|\ |\vec p_2|}

   :p real(kind=prec) p1(4): the first momentum :math:`p_1`
   :p real(kind=prec) p2(4): the second momentum :math:`p_2`

   .. note::

      This will return 0 if the computation fails

Transformations
^^^^^^^^^^^^^^^

.. f:function::  boost_back(rec, mo)

   boosts the momentum ``mo`` from the frame where ``rec`` is at rest to the frame where ``rec`` is specified, i.e.

   .. code:: fortran

        boost_back(rec, (/ 0., 0., 0., sqrt(sq(rec)) /)) = rec

   This function can be viewed as the inversion of :f:func:`boost_rf`.

   :p real(kind=prec) rec(4): the system to boost into
   :p real(kind=prec) mo(4): the momentum to boost
   :r boost_back(4): the boosted momentum
   :rtype boost_back: real(kind=prec)

.. f:function::  boost_rf(rec, mo)

   boosts ``mo`` to (non-unique) rest frame of ``rec``, i.e.

   .. code:: fortran

        boost_rf(rec, rec) = (/ 0., 0., 0., sqrt(sq(rec)) /)

   This function can be viewed as the inversion of :f:func:`boost_back`.

   :p real(kind=prec) rec(4): the system to boost into
   :p real(kind=prec) mo(4): the momentum to boost
   :r boost_back(4): the boosted momentum
   :rtype boost_back: real(kind=prec)

.. f:function:: euler_mat(a,b,c)

   gives the Euler rotation matrix formed by rotation by :math:`\alpha` around the current :math:`z` axis, then by :math:`\beta` around the current :math:`y` axis, and the by :math:`\gamma` around the current :math:`z` axis.

   .. math::

    \begin{pmatrix}
      c_{\alpha}c_{\beta }c_{\gamma}-s_{\alpha}s_{\gamma}&-c_{\alpha}c_{\beta } s_{\gamma}-c_{\gamma}s_{\alpha}&c_{\alpha}s_{\beta }&0\\
      c_{\beta }c_{\gamma}s_{\alpha}+c_{\alpha}s_{\gamma}& c_{\alpha}c_{\gamma}-c_{\beta } s_{\alpha}s_{\gamma}&s_{\alpha}s_{\beta }&0\\
     -c_{\gamma}s_{\beta }                               & s_{\beta }s_{\gamma}                                &c_{\beta }          &0\\
      0                                                  & 0                                                   & 0                  &1
    \end{pmatrix}

   :p real(kind=prec) a: the angle :math:`\alpha`
   :p real(kind=prec) b: the angle :math:`\beta`
   :p real(kind=prec) c: the angle :math:`\gamma`
   :r euler_mat(4,4): the :math:`4\times4` Euler matrix
   :rtype euler_mat: real(kind=prec)


The user file
-------------

Mandatory functions
^^^^^^^^^^^^^^^^^^^

The user must implement the following functions in the user file

.. f:currentmodule:: user

.. f:variable:: nr_q
   :type: integer
   :attrs: parameter = n

   The number of distributions the user intends to calculate

.. f:variable:: nr_bins
   :type: integer
   :attrs: parameter = n

   The number of bins in the distributions the user intends to calculate

.. f:variable:: min_val
   :type: real(kind=prec)
   :shape: nr_q

   The lower bounds of the distributions

.. f:variable:: max_val
   :type: real(kind=prec)
   :shape: nr_q

   The upper bounds of the distributions

.. f:variable:: userdim
   :type: integer

   The number of integrations the user wishes to carry out to account eg. for beam effects

.. f:variable:: pass_cut
   :type: logical
   :shape: nr_q

   This controls whether the event is acceptable.
   If at least one entry of this array is ``.true.`` the event will be calculated and added to the cross section.
   If individual elements are ``.false.``, this event will *not* be added to the corresponding histogram.

   .. note::

     Even though it is possible to calculate multiple closely related cuts simultaneously, this can harm the speed of convergence as the VEGAS algorithm optimises for the cross section and not for the distributions.

.. f:variable:: userweight
   :type: real(kind=prec)

   The weight the user wishes to attach to a given event

.. f:variable:: names
   :type: character(len=namesLen)
   :shape: nr_q

   The names of the distributions the user wishes the calculate

.. f:variable:: filenamesuffix
   :type: character(len=filenamesuffixLen)

   The observable-specific suffix to the vegas file

.. f:subroutine:: fix_mu

   The user needs to choose the renormalisation scale :math:`\mu^2` by writing to the variable :f:var:`musq`.
   This can be done on a per-event basis.

   A common example would be

   .. code:: fortran

      SUBROUTINE FIX_MU
      musq = Mm**2
      END SUBROUTINE FIX_MU

.. f:subroutine:: inituser

   This is called without arguments once as soon as McMule starts and has read all other configuration, meaning that it can access :f:var:`which_piece` and :f:var:`flavour`.
   It may be used to read any further information (like cut configuration etc).
   The user does not have to print hashes -- this is already taken care of -- but is very much invited to include information of what it is they are doing.

   If the user is using the cut channel of the menu, they may need to set the :f:var:`filenamesuffix` variable which is appended to the name of the VEGAS file.

   Example for reading a cut:

   .. code:: fortran

     SUBROUTINE INITUSER
     integer cut
     read*,cut
     write(filenamesuffix,'(I2)') cut
     END SUBROUTINE INITUSER

   with a global variable ``cut``

.. f:function:: quant(q1,q2,q3,q4,q5,q6,q7)

   The :term:`measurement function` the user wishes to calculate.
   This needs to at least set :f:var:`pass_cut` but also returns the values of the observables that are to be computed.
   It usually also calls :f:subr:`fix_mu` to fix the renormalisation scale though this can be done elsewhere.
   If the user wishes to consider polarised scattering, :f:var:`pol1` and :f:var:`pol2` need to be set.

   A minimal example that accepts every event and does not calculate a distribution would be

   .. code:: fortran

     FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)
     real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4),q5(4),q6(4),q7(4)
     real (kind=prec) :: quant(nr_q)
     !! ==== keep the line below in any case ==== !!
     call fix_mu
     pol1 = 0.
     pass_cut = .true.
     END FUNCTION QUANT

   :p real(kind=prec) qi(4): the momenta
   :r quant(nr_q): the observables that are to be histogrammed
   :rtype quant: real(kind=prec)

.. f:subroutine:: userevent(x, ndim)

   The user may use this routine in combination with :f:var:`userweight` to integrate over further parameters, i.e. to calculate

   .. math::

       \sigma \sim \int_0^1 {\rm d} x_1 \int_0^1 {\rm d} x_2 \cdots \int_0^1 {\rm d} x_m
            \times \int {\rm d}\Phi\,\,
            \left\vert\mathcal{M}_n\right\vert^2\,\,
            f(x_1,x_2,\cdots,x_n;p_1,\cdots,p_n)

   with a generalised :term:`measurement function` :math:`f`.
   A minimal example that does not include extra intgration is

   .. code:: fortran

     SUBROUTINE USEREVENT(X, NDIM)
     integer :: ndim
     real(kind=prec) :: x(ndim)
     userweight = 1.
     END SUBROUTINE USEREVENT

   :p real(kind=prec) x(ndim): the values of the integration
   :p integer ndim: the dimension of ``x``, should equal :f:var:`userdim`.


Tweaking parameters
^^^^^^^^^^^^^^^^^^^
In rare cases it may be necessary to tweak some parameters.

.. f:variable:: integer namesLen
   :attrs: parameter = 6

   The maximally allowed length of the histogram :f:var:`names`.

.. f:variable:: integer filenamesuffixLen
   :attrs: parameter = 10

   The maximally allowed length of the observable name as specified in :f:var:`filenamesuffix`.

.. f:variable:: integer bin_kind
   :attrs: parameter = 0

   The binning mechanism being used, ``0`` for :math:`{\rm d}\sigma/{\rm d}Q` and ``1`` for :math:`Q {\rm d}\sigma/{\rm d}Q`.

   .. warning::

     Note that the latter is not properly tested and should only be used with great care

.. _sec_fortran_technical:

Technical routines
------------------

The following types, variables, and routines are unlikely to be needed by the typical user and are instead aimed at McMule's developers.

The particle framework
^^^^^^^^^^^^^^^^^^^^^^

.. f:currentmodule:: global_def

.. f:variable:: integer maxparticles
   :attrs: parameter = 7

   The maximal number of particles allowed

.. f:currentmodule:: functions

.. f:type:: mlm

   :f real(kind=prec) momentum(4): the momentum

.. f:type:: particle

   :f real(kind=prec) momentum(4): the momentum
   :f integer effcharge: the effective charge, corresponding to the ``+charge`` for incoming and ``-charge`` for outgoing particles.
   :f integer charge: the actual charge
   :f logical incoming: ``.true.`` for incoming particles
   :f integer lepcharge: the lepton family (1 for electrons, 2 for muons, 3 for taus), defaults to zero

.. f:type:: particles

   :f type(particle) vec(maxparticles): the constituent partciles
   :f integer n: the number of particles actually used
   :f character(len=1) combo: the flavour combination used, allowed values are ``*`` (any combination), ``x`` (only mixed), ``e`` (only electronic), ``m`` (only muonic), ``t`` (only tauonic)

.. f:function:: make_mlm(qq)

   Construct a :f:type:`mlm`, i.e. a massless momentum

   :p real(kind=prec) qq(4) [in]: the momentum
   :rtype make_mlm: mlm

.. f:function:: part(qq, charge, inc[, lepcharge])

   Construct a :f:type:`particle`.

   :p real(kind=prec) qq(4) [in]: the momentum
   :p integer charge [in]: the charge of the particle
   :p integer inc [in]: +1 for incoming, -1 for outgoing
   :o integer lepcharge [1]: the lepton family number
   :rtype part: particle

.. f:function:: parts(ps[, combo])

   Construct :f:type:`particles` from a list of :f:type:`particle`\ s

   :p type(particle) ps(:) [in]: a list of :f:type:`particle`
   :o character(len=1) combo: the flavour combination used, allowed values are ``*`` (any combination), ``x`` (only mixed), ``e`` (only electronic), ``m`` (only muonic), ``t`` (only tauonic)
   :rtype parts: particles

.. f:function:: eik

   An interface to construct the eikonal factor. :f:type:`eik` can be called with

   - (``kg, pp``), using the type :f:type:`particles`.
     The optional flavour combination ``combo`` restricts the emission to the desired set of fermion lines.
     If ``combo`` is set to ``x``, all contributions but the self-eikonal are included.
   - (``{q1,k1}, kg, {q2,k2}``), with an explicit call to the momenta of the {massive, massless} emitter, before (1) and after (2) the emission.

   :p type(particles) pp [in]: the fermions involved in the photon emission
   :p real(kind=prec) qi(4) [in]: the momenta of the massive emitter
   :p type(mlm) ki [in]: the momenta of the massless emitter
   :p type(mlm) kg [in]: the momentum of the photon
   :r eik: the eikonal factor
   :rtype eik: :f:type: real(kind=prec)

.. f:function:: ieik

   An interface to construct the integrated eikonal factor :cite:`Frederix:2009yq`. :f:type:`ieik` can be called with

   - (``xicut, epcmf, pp[, pole]``), using the type :f:type:`particles`.
     The optional flavour combination ``combo`` restricts the emission to the desired set of fermion lines.
     If ``combo`` is set to ``x``, all contributions but the self-eikonal are included.

   - (``xicut, epcmf, q1, q2[, pole]``), with an explicit call to the momenta of the massive emitter, before (1) and after (2) the emission.

   :p real(kind=prec) xicut [in]: :math:`\xi_c` (cf. Section :ref:`sec_getting_started_nlo`)
   :p real(kind=prec) epcmf [in]: square root of ``scms``
   :p type(particles) pp [in]: the fermions involved in the photon emission
   :p real(kind=prec) qi(4) [in]: the momenta of the massive emitter
   :o real(kind=prec) pole [out]: the singular part of the integrated eikonal, as a coefficient of :math:`1/\epsilon`
   :r ieik: the finite part of the integrated eikonal factor
   :rtype ieik: :f:type: real(kind=prec)

.. f:function:: ntssoft(pp,kk,pole)

   The (universal) soft contribution to the LBK theorem at 1 loop :cite:`Engel:2021ccn`, i.e. the :term:`NTS` soft function.
   The optional flavour combination for the :f:type:`particles` ``pp`` restricts the emission to the desired set of fermion lines. [1]_

   :p type(particles) pp [in]: the fermions involved in the photon emission
   :p real(kind=prec) kk(4) [in]: the momentum of the photon
   :o real(kind=prec) pole [out]: the singular part of the :term:`NTS` soft function, as a coefficient of :math:`1/\epsilon`
   :r ieik: the finite part of the :term:`NTS` soft function
   :rtype ntssoft: :f:type: real(kind=prec)

Matrix element interface
^^^^^^^^^^^^^^^^^^^^^^^^

.. f:function:: partInterface(q1,q2,q3,q4,q5,q6,q7)

   an abstract interface to construct :f:type:`particles` for a given process.

   :p real(kind=prec) qi(4): the momenta
   :r partInterface: the constructed particle string
   :rtype partInterface: :f:type:`particles`

Package-X function
^^^^^^^^^^^^^^^^^^

.. note::

   This section needs to be completed, link to `issue <https://gitlab.com/mule-tools/manual/-/issues/2>`_

.. f:function:: DiscB
.. f:function:: DiscB_cplx
.. f:function:: ScalarC0IR6
.. f:function:: ScalarC0IR6_cplx
.. f:function:: ScalarC0
.. f:function:: ScalarC0_cplx
.. f:function:: ScalarD0IR16
.. f:function:: ScalarD0IR16_cplx

VP  functions
^^^^^^^^^^^^^

.. note::

   This section needs to be completed, link to `issue <https://gitlab.com/mule-tools/manual/-/issues/1>`_

Phase spaces
^^^^^^^^^^^^

.. f:currentmodule:: phase_space

McMule has implemented a number of phase routines that map from the hypercube to the physical momenta.
Here is a list of currently used ones

.. f:subroutine:: PSD3(ra, q1,m1, q2,m2, q3,m3, weight)

   Generic phase space routine for :math:`1\to2` decays

   :p real(kind=prec) ra(2) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD4(ra, q1,m1, q2,m2, q3,m3, q4,m4, weight)

   Generic phase space routine for :math:`1\to3` decays

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD4_FKS(ra, q1,m1, q2,m2, q3,m3, q4, weight)

   :term:`FKS` phase space routine for :math:`1\to3` decays, requires :math:`m_4=0`.
   Tuned for :math:`\sphericalangle(p_2,q_4)` and :math:`E_4`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD5(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5,  weight)

   Generic phase space routine for :math:`1\to4` decays

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD5_25(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5,  weight)

   Phase space routine for :math:`1\to4` decays,
   tuned for :math:`\sphericalangle(p_2,q_5)` and :math:`E_5`, collinear limit is ``ra(2) -> 0``

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD5_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   :term:`FKS` phase space routine for :math:`1\to4` decays, requires :math:`m_5=0`.
   Tuned for :math:`\sphericalangle(p_2,q_5)` and :math:`E_5`

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD6(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6,m6, weight)

   Generic phase space routine for :math:`1\to5` decays

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD6_23_24_34(ra, q1,m1, q2,m2, q5,m5, q6,m6, q3,m3, q4,m4, weight)

   Phase space routine for :math:`1\to5` decays with FKS-ish tuning.
   This is designed for the decay :math:`\mu^+\to e^+\nu\bar\nu e^+e^-`.
   ``q2`` should be the unique particle (electron) and ``q3`` and ``q4`` are the identical particles (postirons)::

                           _/  q
                           /|   2
        ---<---*~~~~~~~~~~*
               |          |
               ^          ^  q
               | q        |   3
                  4

   The 'spectator' neutrinos are ``q5`` and ``q6``.
   Start by generating ``p2`` and ``p3`` at an angle ``* = arccos(y2)``::

                        ^ p2
                       |||
                       |||
          p3         __|||
           --__     /  |||
               --__/  *|||
                   --__|||
                       |||

   Generate ``p4`` at an angle ``* = arccos(y3)`` and rotating by an angle ``phi`` w.r.t. to ``p3``::

                       |||      / p4
                    _--|||--_  /
                   -_  |||  _-/
          p3         --___-- /
           --__        |||__/
               --__    |||*/
                   --__|||/
                       |||

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD6_23_24_34_E56(ra, q1,m1, q2,m2, q5,m5, q6,m6, q3,m3, q4,m4, weight)

   Phase space routine for :math:`1\to5` decays with FKS-ish tuning, similar to :f:subr:`PSD6_23_24_34` but with special tuning on the :math:`E_5+E_6` tail.

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD6_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6, weight)

   :term:`FKS` phase space routine for :math:`1\to5` decays, requires :math:`m_6=0`.
   Tuned for :math:`\sphericalangle(p_2,q_6)` and :math:`E_6`

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD6_25_26_m50_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6, weight)

   :term:`FKS` phase space routine for :math:`1\to5` decays, requires :math:`m_5=m_6=0`.
   Tuned for :math:`\sphericalangle(p_2,q_{5,6})` and :math:`E_{5,6}`

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD6_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   Double-:term:`FKS` phase space routine for :math:`1\to5` decays, requires :math:`m_5=m_6=0`.
   Tuned for :math:`\sphericalangle(p_2,q_{5,6})` and :math:`E_{5,6}`

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD7(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6,m6, q7,m7, weight)

   Generic phase space routine for :math:`1\to6` decays

   :p real(kind=prec) ra(14) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD7_27_37_47_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6,m6, q7, weight)

   :term:`FKS` phase space routine for :math:`1\to6` decays,
   tuned for :math:`\sphericalangle(p_{2,3,4},q_7)` and :math:`E_7`

   :p real(kind=prec) ra(14) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD7_27_37_47_E56_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6,m6, q7, weight)

   :term:`FKS` phase space routine for :math:`1\to6` decays,
   tuned for :math:`\sphericalangle(p_{2,3,4},q_7)` and :math:`E_7` and tuned for the :math:`E_5+E_6` tail

   :p real(kind=prec) ra(14) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX2(ra, q1,m1, q2,m2, q3,m3, q4,m4, weight)

   Generic phase space routine for :math:`2\to2` cross sections

   :p real(kind=prec) ra(2) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   :term:`FKS` phase space routine for :math:`2\to3` cross sections, requires :math:`m_5=0`.
   Tuned for :term:`ISR` and not :term:`FSR`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_35_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight[, sol])

   :term:`FKS` phase space routine for :math:`2\to3` cross sections, requires :math:`m_5=0`.
   Tuned for :math:`\sphericalangle(q_3,q_5)` and :math:`E_5`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian
   :o integer sol [in]: which solution to pick

.. f:subroutine:: PSX4(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6,m6, weight)

   Generic phase space routine for :math:`2\to4` cross sections

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   Double-:term:`FKS` phase space routine for :math:`2\to4` cross sections, requires :math:`m_5=m_6=0`.
   Tuned for :term:`ISR` and not :term`FSR`

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_35_36_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight[, sol])

   Double-:term:`FKS` phase space routine for :math:`2\to4` cross sections, requires :math:`m_5=m_6=0`.
   Tuned for :math:`\sphericalangle(q_3,q_{5,6})` and :math:`E_{5,6}`

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian
   :o integer sol [in]: which solution to pick

.. f:subroutine:: PSD6_P_25_26_m50_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6, weight)

   :term:`FKS` phase space routine for :math:`1\to5` decays, requires :math:`m_5=m_6=0`.
   Tuned for :math:`\sphericalangle(p_2,q_{5,6})` and :math:`E_{5,6}`
   Partioning of :f:subr:`PSD6_25_26_m50_FKS` with :math:`s_{26}<s_{25}`.

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD6_26_2x5(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6,m6, weight)

   Phase space routine for :math:`1\to5` decays with FKS-ish tuning.
   Modification of :f:subr:`PSD6_23_24_34` with :math:`2\leftrightarrow 5`.
   This is designed for the decay :math:`\mu^+\to e^+\nu\bar\nu e^+e^-`.

   :p real(kind=prec) ra(11) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSD7_27_37_47_2x5_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5,m5, q6,m6, q7, weight)

   :term:`FKS` phase space routine for :math:`1\to6` decays,
   tuned for :math:`\sphericalangle(p_{2,3,4},q_7)` and :math:`E_7`
   Modification of :f:subr:`PSD7_27_37_47_FKS` with :math:`2\leftrightarrow 5`.

   :p real(kind=prec) ra(14) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_P_15_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   :term:`FKS` phase space routine for :math:`2\to3` cross sections, requires :math:`m_5=0`.
   Tuned for :term:`ISR` and not :term:`FSR`.
   Partioning of :f:subr:`PSX3_FKS` with :math:`s_{15}<s_{35}`.

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_P13_35_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   :term:`FKS` phase space routine for :math:`2\to3` cross sections, requires :math:`m_5=0`.
   Tuned for :math:`\sphericalangle(q_3,q_5)` and :math:`E_5`
   Partioning of :f:subr:`PSX3_35_FKS` with :math:`s_{15}>s_{35}`.

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_coP13_35_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   The corner piece to :f:subr:`PSX3_P13_35_FKS`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_P_15_25_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   :term:`FKS` phase space routine for :math:`2\to3` cross sections, requires :math:`m_5=0`.
   Tuned for :term:`ISR` and not :term:`FSR`.
   Partioning of :f:subr:`PSX3_FKS` with :math:`{\rm min}\big(s_{15},s_{25}\big)<{\rm min}\big(s_{35},s_{45}\big)`.

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_P_35_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   :term:`FKS` phase space routine for :math:`2\to3` cross sections, requires :math:`m_5=0`.
   Tuned for :math:`\sphericalangle(q_3,q_5)` and :math:`E_5`
   Partioning of :f:subr:`PSX3_35_FKS` with :math:`s_{35}<{\rm min}\big(s_{15},s_{25},s_{45}\big)`.

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_P_45_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   :term:`FKS` phase space routine for :math:`2\to3` cross sections, requires :math:`m_5=0`.
   Tuned for :math:`\sphericalangle(q_3,q_5)` and :math:`E_5`
   Partioning of :f:subr:`PSX3_35_FKS` with :math:`s_{45}<{\rm min}\big(s_{15},s_{25},s_{35}\big)` and :math:`3\leftrightarrow4`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_coP_35_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   The corner piece to :f:subr:`PSX3_P_35_FKS`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX3_coP_45_FKS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, weight)

   The corner piece to :f:subr:`PSX3_P_45_FKS`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_P_15_16_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   Double-:term:`FKS` phase space routine for :math:`2\to4` cross sections, requires :math:`m_5=m_6=0`.
   Tuned for :term:`ISR` and not :term`FSR`
   Partioning of :f:subr:`PSX4_FKSS` with :math:`{\rm min}\big(s_{15}, s_{16}\big)<{\rm min}\big(s_{35},s_{36}\big)`.

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_P_35_36_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   Double-:term:`FKS` phase space routine for :math:`2\to4` cross sections, requires :math:`m_5=m_6=0`.
   Tuned for :math:`\sphericalangle(q_3,q_{5,6})` and :math:`E_{5,6}`
   Partioning of :f:subr:`PSX4_35_36_FKSS` with :math:`{\rm min}\big(s_{15}, s_{36}\big)<{\rm min}\big(s_{15},s_{25},s_{45},s_{16},s_{26},s_{46}\big)`.

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_coP_35_36_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   The corner piece to :f:subr:`PSX4_P_35_36_FKSS`

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_P13_35_36_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   Double-:term:`FKS` phase space routine for :math:`2\to4` cross sections, requires :math:`m_5=m_6=0`.
   Tuned for :math:`\sphericalangle(q_3,q_{5,6})` and :math:`E_{5,6}`
   Partioning of :f:subr:`PSX4_35_36_FKSS` with :math:`{\rm min}\big(s_{15}, s_{16}\big)>{\rm min}\big(s_{35},s_{36}\big)`.

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_coP13_35_36_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   The corner piece to :f:subr:`PSX4_P13_35_36_FKSS`

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_P_15_16_25_26_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   Double-:term:`FKS` phase space routine for :math:`2\to4` cross sections, requires :math:`m_5=m_6=0`.
   Tuned for :term:`ISR` and not :term`FSR`
   Partioning of :f:subr:`PSX4_FKSS` with :math:`{\rm min}\big(s_{15}, s_{16},s_{25}, s_{26}\big)<{\rm min}\big(s_{35},s_{36},s_{54},s_{46}\big)`.

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_P_45_46_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   Double-:term:`FKS` phase space routine for :math:`2\to4` cross sections, requires :math:`m_5=m_6=0`.
   Tuned for :math:`\sphericalangle(q_3,q_{5,6})` and :math:`E_{5,6}`
   Partioning of :f:subr:`PSX4_35_36_FKSS` with :math:`{\rm min}\big(s_{45}, s_{46}\big)>{\rm min}\big(s_{15},s_{25},s_{35},s_{16},s_{26},s_{36}\big)`.

   :p real(kind=prec) ra(8) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. f:subroutine:: PSX4_coP_45_46_FKSS(ra, q1,m1, q2,m2, q3,m3, q4,m4, q5, q6, weight)

   The corner piece to :f:subr:`PSX4_P_45_46_FKSS`

   :p real(kind=prec) ra(5) [in]: the random numbers
   :p real(kind=prec) qi(4) [out]: the momenta
   :p real(kind=prec) mi [in]: the masses
   :p real(kind=prec) weight [out]: the Jacobian

.. [1]
   The user is allowed to further split mixed contributions at NNLO, i.e. contributions with emissions connecting different fermion lines.
   This is achieved via the optional parameter ``mx`` of the auxiliary function :f:type:`combonts`.
   The latter sets the desired flavour combination for :f:type:`ntssoft`, and ``mx=1`` allows to choose among different mixed contributions.
   For example, for :math:`\ell_1\ell_2\to\ell_1\ell_2` scattering, if a formal charge :math:`Q_{1(2)}` is assigned for each photon emission from :math:`\ell_{1(2)}`, :f:type:`ntssoft` will be able to distinguish among the contributions labelled by :math:`Q_1^5\,Q_2^3`, :math:`Q_1^4\,Q_2^4` and :math:`Q_1^3\,Q_2^5`.
