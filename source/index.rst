.. McMule documentation master file, created by
   sphinx-quickstart on Tue Nov 15 13:47:33 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

McMule
==================================

.. authors:: CITATION.cff

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting-started.rst
   structure.rst
   general.rst
   technical.rst
   new.rst
   fks.rst
   glossary.rst
   bibliography.rst
   pid.rst
   pieces.rst
   fortran.rst
   pymule.rst
   pymule_full.rst

(**M**\ onte **c**\ arlo for **Mu**\ ons and other **le**\ ptons) is a generic framework for higher-order QED calculations of scattering and decay processes involving leptons.
It is written in Fortran 95 with two types of users in mind.
First, several processes are implemented, some at :term:`NLO`, some at :term:`NNLO`.
For these processes, the user can define an arbitrary (infrared safe), fully differential observable and compute cross sections and distributions.
McMule's processes, present and, future, are listed in :numref:`tab_mcmuleprocs` together with the relevant experiments for which the cuts are implemented.
Second, the program is set up s.t. additional processes can be implemented by supplying the relevant matrix elements.

.. _tab_mcmuleprocs:
.. table:: Processes implemented in McMule
   :widths: auto

   ====================================== ========= =============== =============================
   **process**                            **order** **experiments** **comments**
   :math:`\mu\to\nu\bar\nu e`             NNLO      MEG I&II        polarised, massified & exact
   :math:`\mu\to\nu\bar\nu e\gamma`       NLO       MEG I           polarised
   :math:`\mu\to\nu\bar\nu eee`           NLO       Mu3e            polarised
   :math:`\mu\to\nu\bar\nu e\gamma\gamma` LO        MEG             polarised
   :math:`\tau\to\nu\bar\nu e\gamma`      NLO       BaBar           cuts in lab frame
   :math:`\tau\to\nu\bar\nu l\ell\ell`    NLO       Belle II
   \                                      NLO       MUonE
   \                                                NNLO            purely electronic corrections
   \                                                                mixed (massified)
   :math:`\ell p\to\ell p`                NNLO      P2, MUSE, Prad  only leptonic corrections
   :math:`e^-e^-\to e^-e^-`               NNLO      Prad            complete
   :math:`e^+e^-\to e^+e^-`               NNLO                      no :math:`n_f`
   :math:`e^+e^-\to \gamma\gamma`         NNLO      PADME
   :math:`e^+e^-\to \mu^+\mu^-`           NNLO      Belle           massified
   ====================================== ========= =============== =============================

The public version of the code can be found at

   https://gitlab.com/mule-tools/mcmule

To obtain a copy of the code, ``git`` is recommended

.. code:: bash

   $ git clone --recursive https://gitlab.com/mule-tools/mcmule

Alternatively, we provide a Docker :term:`container` for easy deployment and legacy results (cf. Section :ref:`sec_docker`).
In multi-user environments, **udocker** can be used instead.
In either case, a pre-compiled copy of the code can be obtained by calling

.. code:: bash

   $ docker pull yulrich/mcmule  # requires Docker to be installed
   $ udocker pull yulrich/mcmule # requires udocker to be installed

We provide instructions on how is used in Section :ref:`sec_getting_started`.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
