.. _sec_pieces:

Available processes and :f:var:`which_piece`
============================================

When running McMule, we recommend using the following :f:var:`which_piece`

.. table:: :f:var:`which_piece` to use for different physics processes
   :widths: auto

   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | Process                           | Order  | :math:`n`-particle | :math:`(n+1)`-particle | :math:`(n+2)`-particle |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`\mu\to e\nu\bar\nu`        | LO     | ``m2enn0``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``m2ennF``         | ``m2ennR``             |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NNLO   | ``m2ennFF``,       | ``m2ennRF``            | ``m2ennRR``            |
   |                                   |        | ``m2ennNF``        |                        |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`\mu\to e\nu\bar\nu \gamma` | LO     | ``m2enng0``        |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``m2enngF``        | ``m2enngR``            |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`\mu\to e\nu\bar\nu ee`     | LO     | ``m2ennee0``       |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``m2enneeV``,      | ``m2enneeR``           |                        |
   |                                   |        | ``m2enneeC``,      |                        |                        |
   |                                   |        | ``m2enneeA``,      |                        |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`\mu\to eJ`                 | LO     | ``m2ej0``          |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``m2ejF``          | ``m2ejR``              |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`\mu\to eJ\gamma`           | LO     | ``m2ejg0``         |                        |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`e\mu \to e\mu`             | LO     | ``em2em0``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``em2emFEE``,      | ``em2emREE15``,        |                        |
   |                                   |        | ``em2emFEM``,      | ``em2emREE35``,        |                        |
   |                                   |        | ``em2emFMM``,      | ``em2emREM``,          |                        |
   |                                   |        | ``em2emA``         | ``em2emRMM``           |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | el.    | ``em2emFFEEEE``,   | ``em2emRFEEEE15``,     | ``em2emRREEEE1516``,   |
   |                                   | NNLO   | ``em2emAA``,       | ``em2emRFEEEE35``,     | ``em2emRREEEE3536``,   |
   |                                   |        | ``em2emAFEE``,     | ``em2emRFEEEEco``,     | ``em2emRREEEEc``       |
   |                                   |        | ``em2emNFEE``      | ``em2emAREE15``,       |                        |
   |                                   |        |                    | ``em2emAREE35``        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | full   | ``em2emFFMIXDz``,  | ``em2emRFMIXD15``,     | ``em2emRRMIXD1516``,   |
   |                                   | NNLO   | ``em2emFFMMMM``,   | ``em2emRFMIXD35``,     | ``em2emRRMIXD3536``,   |
   |                                   |        | ``em2emAFEM``,     | ``em2emRFMIXDco``,     | ``em2emRRMIXDc``,      |
   |                                   |        | ``em2emAFMM``,     | ``em2emRFMMMM``,       | ``em2emRRMMMM``        |
   |                                   |        | ``em2emNFEM``,     | ``em2emAREM``,         |                        |
   |                                   |        | ``em2emNFMM``      | ``em2emARMM``          |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`\mu p\to \mu p`            | LO     | ``mp2mp0``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``mp2mpF``,        | ``mp2mpR15``,          |                        |
   |                                   |        | ``mp2mpA``         | ``mp2mpR35``           |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NNLO   | ``mp2mpFF``,       | ``mp2mpRF15``,         | ``mp2mpRR1516``,       |
   |                                   |        | ``mp2mpAA``,       | ``mp2mpRF35``,         | ``mp2mpRR3536``,       |
   |                                   |        | ``mp2mpAF``,       | ``mp2mpRFco``,         | ``mp2mpRRco``          |
   |                                   |        | ``mp2mpNF``        | ``mp2mpAR15``,         |                        |
   |                                   |        |                    | ``mp2mpAR35``          |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`ee\to\mu\mu`               | LO     | ``ee2mm0``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``ee2mmF``,        | ``ee2mmR``             |                        |
   |                                   |        | ``ee2mmA``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | el.    | ``ee2mmFFEEEE``,   | ``ee2mmRFEEEE``,       | ``ee2mmRREEEE``,       |
   |                                   | NNLO   | ``ee2mmAA``,       | ``ee2mmAREE``          |                        |
   |                                   |        | ``ee2mmAFEE``,     |                        |                        |
   |                                   |        | ``ee2mmNFEE``      |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | LO     | ``eeZmm0``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | EW NLO | ``eeZmmFX``,       | ``eeZmmRX``            |                        |
   |                                   |        | ``eeZmmAX``        |                        |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`e^-e^- \to e^-e^-`         | LO     | ``ee2ee0``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``ee2eeF``,        | ``ee2eeR125``,         |                        |
   |                                   |        | ``ee2eeA``         | ``ee2eeR345``          |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NNLO   | ``ee2eeFF``,       | ``ee2eeRF125``,        | ``ee2eeRR15162526``,   |
   |                                   |        | ``ee2eeAF``,       | ``ee2eeRF345``,        | ``ee2eeRR35364546``    |
   |                                   |        | ``ee2eeAA``,       | ``ee2eeAF125``,        |                        |
   |                                   |        | ``ee2eeNF``        | ``ee2eeAR135``         |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+
   | :math:`e^-e^+ \to e^-e^+`         | LO     | ``eb2eb0``         |                        |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NLO    | ``eb2ebF``,        | ``eb2ebR125``,         |                        |
   |                                   |        | ``eb2ebA``         | ``eb2ebR35``,          |                        |
   |                                   |        |                    | ``eb2ebR45``           |                        |
   |                                   +--------+--------------------+------------------------+------------------------+
   |                                   | NNLO   | ``eb2ebFF``,       | ``eb2ebRF125``,        | ``eb2ebRR15162526``,   |
   |                                   |        | ``eb2ebAF``,       | ``eb2ebRF35``,         | ``eb2ebRR3536``,       |
   |                                   |        | ``eb2ebAA``,       | ``eb2ebRF45``,         | ``eb2ebRR4546``        |
   |                                   |        | ``eb2ebNF``        | ``eb2ebAR125``,        |                        |
   |                                   |        |                    | ``eb2ebAR35``,         |                        |
   |                                   |        |                    | ``eb2ebAR45``          |                        |
   +-----------------------------------+--------+--------------------+------------------------+------------------------+

We also show a list of all available :f:var:`which_piece` in :numref:`fig_proctree`.

.. _fig_proctree:
.. mermaid:: figures/pieces.mmd
   :caption: The :f:var:`which_piece` implemented in McMule
