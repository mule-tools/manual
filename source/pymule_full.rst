.. _sec_pymule_full:

pymule reference guide
======================

This section describes all functions and classes in pymule.
Most users will not have to view this.

Working with errors
-------------------

.. automodule:: pymule.errortools
   :members:

Working with abstract records
-----------------------------

.. automodule:: pymule.record
   :members:

Working with vegas records
-----------------------------

.. automodule:: pymule.vegas
   :members:

Working with records of data
----------------------------

.. automodule:: pymule.loader
   :members:

Working with :math:`\xi_c` data
-------------------------------

.. automodule:: pymule.xicut
   :members:

Working with plots
------------------

.. automodule:: pymule.plot
   :members:

.. automodule:: pymule.colours
   :members:

.. automodule:: pymule.mule
   :members:

.. automodule:: pymule.mpl_axes_aligner
   :members:

Useful other functions
----------------------
.. automodule:: pymule.compress
   :members:

.. automodule:: pymule.maths
   :members:
