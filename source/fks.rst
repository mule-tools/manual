.. _sec_fks:

The FKS\ :math:`^2` scheme
==========================

In the following we very briefly review the :term:`FKS` :cite:`Frixione:1995ms, Frederix:2009yq` and :term:`FKS`:math:`^2` schemes :cite:`Engel:2019nfw` though this is not meant as an introduction into these schemes.
For this see :cite:`Ulrich:2019fks, Engel:2019nfw, Ulrich:2020phd`.
Here, we just give a schematic overview with the basic information required to understand the structure of the code.

The core idea of this method is to render the phase-space integration of a real matrix element finite by subtracting all possible soft limits.
The subtracted pieces are partially integrated over the phase space and combined with the virtual matrix elements to form finite integrands.

The :term:`NLO` corrections :math:`\sigma^{(1)}` to a cross section are split into a :math:`n` particle and :math:`(n+1)` particle contribution and are written as

.. math::
   :label: eq_nlo

      \sigma^{(1)} =
      \sigma^{(1)}_n(\xi_{c}) + \sigma^{(1)}_{n+1}(\xi_{c})

.. math::
   :label: eq_nlo_n

      \sigma^{(1)}_n(\xi_{c}) = \int
      \ \mathrm{d}\Phi_n^{d=4}\,\Bigg(
          \mathcal{M}_{n}^{(1)}
         +\hat{\mathcal{E}}(\xi_{c})\,\mathcal{M}_{n}^{(0)}
      \Bigg) = 
      \int \ \mathrm{d}\Phi_n^{d=4}\, \mathcal{M}_{n}^{(1)f}

.. math::
   :label: eq_nlo_n1

      \sigma^{(1)}_{n+1}(\xi_{c}) = \int 
      \ \mathrm{d}\Phi^{d=4}_{n+1}
        \left(\frac{1}{\xi_1}\right)_{c} \big(\xi_1\, \mathcal{M}_{n+1}^{(0)f} \big)

In :eq:`eq_nlo_n1`, :math:`\xi_1` is a variable of the :math:`(n+1)` parton phase space :math:`\mathrm{d}\Phi^{d=4}_{n+1}` that corresponds to the (scaled) energy of the emitted photon.
For :math:`\xi_1\to 0` the real matrix element :math:`\mathcal{M}_{n+1}^{(0)f}` develops a singularity.
The superscripts :math:`(0)` and :math:`f` indicate that the matrix element is computed at tree level and is finite, i.e.  free of explicit infrared poles :math:`1/\epsilon`.
In order to avoid an implicit infrared pole upon integration, the :math:`\xi_1` integration is modified by the factor :math:`\xi_1 (1/\xi_1)_c`, where the distribution :math:`(1/\xi_1)_c` acts on a test function :math:`f` as

.. math::
   :label: eq_xidist

   \int_0^1\mathrm{d}\xi_1\, \left(\frac{1}{\xi_1}\right)_{c}\, f(\xi_1)
   \equiv
   \int_0^1\mathrm{d}\xi_1\,\frac{f(\xi_1)-f(0)\theta(\xi_{c}-\xi_1)}{\xi_1}

Thus, for :math:`\xi_1 < \xi_{c}`, the integrand is modified through the subtraction of the soft limit.
This renders the integration finite.
However, it also modifies the result.
The missing piece of the real corrections can be trivially integrated over :math:`\xi_1`.
This results in the integrated eikonal factor :math:`\hat{\mathcal{E}}(\xi_{c})` times the tree-level matrix element for the :math:`n` particle process, :math:`\mathcal{M}_{n}^{(0)}`.
The factor :math:`\hat{\mathcal{E}}(\xi_{c})` has an explicit :math:`1/\epsilon` pole that cancels precisely the corresponding pole in the virtual matrix element :math:`\mathcal{M}_{n}^{(1)}`.
Thus, the combined integrand of :eq:`eq_nlo_n` is free of explicit poles, hence denoted by :math:`\mathcal{M}_{n}^{(1)f}`, and can be integrated numerically over the :math:`n` particle phase space :math:`\mathrm{d}\Phi_n^{d=4}`.

The parameter :math:`\xi_{c}` that has been introduced to split the real corrections can be chosen arbitrarily as long as

.. math::
   :label: eq_xic

   0<\xi_{c}\le\xi_{\text{max}} = 1-\frac{\big(\sum_i m_i\big)^2}{s}

where the sum is over all masses in the final state.
The :math:`\xi_{c}` dependence has to cancel exactly between :eq:`eq_nlo_n` and :eq:`eq_nlo_n1` since at no point any approximation was made in the integration.
Checking this independence is a very useful tool to test the implementation of the method, as well as its numerical stability.

The finite matrix element :math:`\mathcal{M}_{n}^{(1)f}` is simply the first-order expansion of the general YFS exponentiation formula for soft singularities

.. math::
   :label: eq_yfsnew

   e^{\hat{\mathcal{E}}}\, \sum_{\ell = 0}^\infty \mathcal{M}_{n}^{(\ell)} = 
   \sum_{\ell = 0}^\infty \mathcal{M}_{n}^{(\ell)f}
   =  \mathcal{M}_{n}^{(0)} +
     \Big(\mathcal{M}_{n}^{(1)} +\hat{\mathcal{E}}(\xi_{c})\,\mathcal{M}_{n}^{(0)}\Big) + \mathcal{O}(\alpha^2)

where we exploited the implicit factor :math:`\alpha` in :math:`\hat{\mathcal{E}}`.

For QED with massive fermions this scheme can be extended to :term:`NNLO` and, in fact beyond.
The :term:`NNLO` corrections are split into three parts

.. math::
   :label: eq_nnlo_n

      \sigma^{(2)}_n(\xi_{c}) = \int
      \ \mathrm{d}\Phi_n^{d=4}\,\bigg(
          \mathcal{M}_{n}^{(2)}
         +\hat{\mathcal{E}}(\xi_{c})\,\mathcal{M}_{n}^{(1)}
         +\frac1{2!}\mathcal{M}_{n}^{(0)} \hat{\mathcal{E}}(\xi_{c})^2
      \bigg) = 
      \int \ \mathrm{d}\Phi_n^{d=4}\, \mathcal{M}_{n}^{(2)f}

.. math::
   :label: eq_nnlo_n1

      \sigma^{(2)}_{n+1}(\xi_{c}) = \int 
      \ \mathrm{d}\Phi^{d=4}_{n+1}
        \left(\frac{1}{\xi_1}\right)_c \Big(\xi_1\, \mathcal{M}_{n+1}^{(1)f}(\xi_{c})\Big)

.. math::
   :label: eq_nnlo_n2

      \sigma^{(2)}_{n+2}(\xi_{c}) = \int
      \ \mathrm{d}\Phi_{n+2}^{d=4}
         \left(\frac{1}{\xi_1}\right)_c\,
         \left(\frac{1}{\xi_2}\right)_c\,
           \Big(\xi_1\xi_2\, \mathcal{M}_{n+2}^{(0)f}\Big)

Thus we have to evaluate :math:`n` parton contributions, single-subtracted :math:`(n+1)` parton contributions, and double-subtracted :math:`(n+2)` parton contributions.
This structure will be mirrored in the Fortran code.
The :math:`\xi_{c}` dependence cancels, once all three contributions are taken into account.
For this subtraction method we need the matrix elements with massive fermions.
If the two-loop amplitudes are available only for massless fermions, it is possible to use massification :cite:`Engel:2018fsb`.

.. _sec:nllo:

FKS\ :math:`^\ell`: extension to N\ :math:`^\ell`\ LO
-----------------------------------------------------

The pattern that has emerged in the previous cases leads to the following extension to an arbitrary order :math:`\ell` in perturbation theory:


.. math::
   :label: eq_nellocomb_a

      \mathrm{d}\sigma^{(\ell)} = \sum_{j=0}^\ell \mathrm{d}\sigma^{(\ell)}_{n+j}(\xi_{c})

.. math::
  :label: eq_nellocomb_b

      \mathrm{d}\sigma^{(\ell)}_{n+j}(\xi_{c}) =  \mathrm{d}\Phi_{n+j}^{d=4}\,\frac{1}{j!} \, 
      \bigg( \prod_{i=1}^j \left(\frac{1}{\xi_i}\right)_c \xi_i \bigg)\,
       \mathcal{M}_{n+j}^{(\ell-j)f}(\xi_{c})

The eikonal subtracted matrix elements

.. math::

   \mathcal{M}_{m}^{(\ell )f}= \sum_{j=0}^\ell\frac{\hat{\mathcal{E}}^j}{j!} \mathcal{M}_{m}^{(\ell-j)}

(with the special case :math:`\mathcal{M}_{m}^{(0)f} = \mathcal{M}_{m}^{(0)}` included) are free from :math:`1/\epsilon` poles, as indicated in :eq:`eq_yfsnew`.
Furthermore, the phase-space integrations are manifestly finite.
