<<qgraf.wl
onshell = {
  p.p -> M^2, q.q -> m^2, p.q -> s/2
};

A0 = (4GF/Sqrt[2]) "diag1"/.RunQGraf[{"mum"},{"nu","elm"},0] //. {
  line[_, x_] -> x, p1->p, q1->p-q, q2->q, _δZ | δm -> 0
};
A1 = pref /. RunQGraf[{"mum"},{"nu","elm"},1] //. {
  line[_, x_] -> x, p1->p, q1->p-q, q2->q, _δZ | δm -> 0
};

M0=Block[{Dim=4},Simplify[Contract[
  1/2 Z2[m] Z2[M] FermionSpinSum[
    A0 /. DiracPL -> (Dirac1 - Z5 γ5)/2,
    A0 /. DiracPL -> (Dirac1 + Z5 γ5)/2
  ]
]] /. onshell]/.{
  Z2[M_] -> 1 + (α/(4π)) (-3/(2ε)-5/2 + 3/2 Log[M^2/Mu^2]),
  Z5     -> 1 - (α/(4π))
};
M1=Block[{Dim=4},Simplify[Contract[
  1/2 FermionSpinSum[
    A1/.γ.k1 -> γ. 4[k1]+I γ5 μ,
    A0
  ]
] /. onshell /. {
  μ^n_ /; EvenQ[n] -> μ2^(n/2), μ -> 0
}/.{
  4[k1]. 4[k1] -> k1.k1 + μ2, 4[k1] -> k1
}]];

M1bare = Simplify[KallenExpand[LoopRefine[LoopRelease[
  Pro2LoopIntegrate[
    Coefficient[M1, μ2, 0]/(16 π^2)
  ]
  + μIntegrate[
    Coefficient[M1, μ2, 1]/(64 π^3),
    1
  ],
  onshell
]]] /. e -> Sqrt[4 πα]];
