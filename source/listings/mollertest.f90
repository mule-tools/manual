SUBROUTINE TESTEEMATEL
implicit none
real (kind=prec) :: x(2),y(5)
real (kind=prec) :: single, finite, lin
real (kind=prec) :: weight
integer ido

call blockstart("ee matrix elements")
scms = 40000.
musq = me
x = (/0.75,0.5/)
call ps_x2(x,scms,p1,me,p2,me,p3,me,p4,me,weight)
call check("ee2ee" ,ee2ee (p1,p2,p3,p4), 2.273983244890001e4, threshold=2e-8)
call check("ee2eel",ee2eel(p1,p2,p3,p4), 6.964297070440638e7, threshold=2e-8)

scms = 40000.
y = (/0.3,0.6,0.8,0.4,0.9/)
call ps_x3_fks(y,scms,p1,me,p2,me,p3,me,p4,me,p5,weight)
call check("ee2eeg",ee2eeg(p1,p2,p3,p4,p5),7.864297444955537e2, threshold=2e-8)

call blockend(3)
END SUBROUTINE

SUBROUTINE TESTMEEVEGAS
xinormcut1 = 0.2
xinormcut2 = 0.3

call blockstart("Moller VEGAS test")

call test_INT('ee2ee0', sigma_0, 2,10,10, NaN)
call test_INT('ee2eeF', sigma_0, 2,10,10, NaN)
call test_INT('ee2eeR', sigma_1, 5,10,10, NaN)
call blockend(3)
END SUBROUTINE
