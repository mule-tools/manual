                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE EE
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use ee_mat_el
  implicit none
  contains

  FUNCTION EE2EE_part(p1, p2, p3, p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! both massive and massless electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: ee2ee_part
  ee2ee_part = parts((/part(p1, 1, 1), part(p2, 1, 1), part(p3, 1, -1), part(p4, 1, -1)/))
  END FUNCTION EE2EE_part

                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE EE
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
