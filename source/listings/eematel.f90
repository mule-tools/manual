                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE EE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use ee_ee2eel, only: ee2eel
  use ee_ee2eeg, only: ee2eeg
  implicit none

  contains

  FUNCTION EE2EE(p1,p2,q1,q2)
    !! e-(p1) e-(p2) -> e-(q1) e-(q2)
    !! for massive electrons
  ...
  END FUNCTION EE2EE

  FUNCTION EE2EEF(p1,p2,q1,q2)
    !! e-(p1) e-(p2) -> e-(q1) e-(q2)
    !! massive electrons
  real(kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real(kind=prec) :: ee2eef, mat0, Epart

  Epart = sqrt(sq(p1+p2))
  mat0 = ee2ee(p1,p2,q1,q2)

  ee2eef = ee2eel(p1,p2,q1,q2) + alpha / (2 * pi) * mat0 * (&
      - Ieik(xieik1,Epart,p1,p2) + Ieik(xieik1,Epart,p1,q1) &
      + Ieik(xieik1,Epart,p1,q2) + Ieik(xieik1,Epart,p2,q1) &
      + Ieik(xieik1,Epart,p2,q2) - Ieik(xieik1,Epart,q1,q2))

  END FUNCTION EE2EEF
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE EE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
