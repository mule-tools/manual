.. _sec_general:

General aspects of using McMule
===============================

In this section, we will collect a few general points of interest regarding McMule.
In particular, we will discuss heuristics on how much statistics is necessary for different contributions in Section :ref:`sec_stat`.
This is followed by a more in-depth discussion of the analysis strategy in Section :ref:`sec_analysis`.

.. _sec_stat:

Statistics
----------

McMule is a Monte Carlo program.
This means it samples the integrand at :math:`N` (pseudo-)random points to get an estimate for the integral.
However, because it uses the adaptive Monte Carlo integration routine ``vegas`` :cite:`Lepage:1980jk`, we split :math:`N=i\times n` into :math:`i` iterations (``itmx``), each with :math:`n` points (``nenter``).
After each iteration, ``vegas`` changes the way it will sample the next iteration based on the results of the previous one.
Hence, the performance of the integration is a subtle interplay between :math:`i` and :math:`n` – it is not sufficient any more to consider their product :math:`N`.

Further, we always perform the integration in two steps: a pre-conditioning with :math:`i_{\text{ad}}\times n_{\text{ad}}` (``nenter_ad`` and ``itmx_ad``, respectively), that is used to optimise the integration strategy and after which the result is discarded, and a main integration that benefits from the integrator’s understanding of the integrand.

Of course there are no one-size-fits-all rules of how to choose the :math:`i` and :math:`n` for pre-conditioning and main run.
However, the following heuristics have proven helpful:

-  :math:`n` is always much larger than :math:`i`.
   For very simple integrands, :math:`n=\mathcal{O}(10\cdot 10^3)` and :math:`i=\mathcal{O}(10)`.

-  Increasing :math:`n` reduces errors that can be thought of as systematic because it allows the integrator to ‘discover’ new features of the integrand.
   Increasing :math:`i` on the other hand will rarely have that effect and only improves the statistical error.
   This is especially true for distributions

-  There is no real limit on :math:`n`, except that it has to fit into the datatype used – integrations with :math:`n=\mathcal{O}(2^{31}-1)` are not too uncommon – while :math:`i` is rarely (much) larger than 100.

-  For very stringent cuts it can happen that that typical values of :math:`n_{\text{ad}}` are too small for any point to pass the cuts.
   In this case ``vegas`` will return ``NaN``, indicating that no events were found.
   Barring mistakes in the definition of the cuts, a pre-pre-conditioning with extremely large :math:`n` but :math:`i=1\!-\!2` can be helpful.

-  :math:`n` also needs to be large enough for ``vegas`` to reliably find all features of the integrand.
   It is rarely obvious that it did, though sometimes it becomes clear when increasing :math:`n` or looking at intermediary results as a function of the already-completed iterations.

-  The main run should always have larger :math:`i` and :math:`n` than the pre-conditioning.
   Judging how much more is a delicate game though :math:`i/i_{\text{ad}} = \mathcal{O}(5)` and :math:`n/n_{\text{ad}} = \mathcal{O}(10\!-\!50)` have been proven helpful.

-  If, once the integration is completed, the result is unsatisfactory, take into account the following strategies

   -  A large :math:`\chi^2/\rm{d.o.f.}` indicates a too small :math:`n`.
      Try to increase :math:`n_{\text{ad}}` and, to a perhaps lesser extent, :math:`n`.

   -  Increase :math:`i`.
      Often it is a good idea to consciously set :math:`i` to a value so large that the integrator will never reach it and to keep looking at ‘intermediary’ results.

   -  If the error is small enough for the application but the result seems incorrect (for example because the :math:`\xi_{c}` dependence does not vanish), massively increase :math:`n`.

-  Real corrections need much more statistics in both :math:`i` and :math:`n` (:math:`\mathcal{O}(10)` times more for :math:`n`, :math:`\mathcal{O}(2)` for :math:`i`) than the corresponding :term:`LO` calculations because of the higher-dimensional phase-space.

-  Virtual corrections have the same number of dimensions as the :term:`LO` calculation and can go by with only a modest increase to account for the added functional complexity.

-  ``vegas`` tends to underestimate the numerical error.

These guidelines are often helpful but should not be considered infallible as they are just that – guidelines.

McMule is not parallelised; however, because Monte Carlo integrations require a :term:`random seed` anyway, it is possible to calculate multiple estimates of the same integral using different :term:`random seeds<random seed>` :math:`z_1` and combining the results obtained this way.
This also allows to for a better, more reliable understanding of the error estimate.

.. _sec_analysis:

Analysis
--------

Once the Monte Carlo has run, an offline analysis of the results is required.
This entails loading, averaging, and combining the data.
This is automatised in pymule but the basic steps are

0. Load the data into a suitable analysis framework such as ``python``.

1. Combine the different :term:`random seeds<random seed>` into one result per contribution and :math:`\xi_{c}`.
   The :math:`\chi^2/{\rm d.o.f.}` of this merging must be small.
   Otherwise, try to increase the statistics or choose of different phase-space parametrisation.

2. Add all contributions that combine into one of the physical contributions :eq:`eq_nellocomb_b`.
   This includes any partitioning done in Section :ref:`sec_ps`.

3. (optional) At N\ :math:`^\ell`\ LO, perform a fit\ [1]_

   .. math::
      :label: eq_xifit

      \sigma_{n+j}^{(\ell)}
         = c_0^{(j)}
         + c_1^{(j)} \log\xi_{c}
         + c_2^{(j)} \log^2\xi_{c}
         + \cdots
         + c_\ell^{(j)} \log^\ell
         = \sum_{i=0}^\ell c_i^{(j)}\log^i\xi_{c}

   This has the advantage that it very clearly quantifies any residual :math:`\xi_{c}` dependence.
   We will come back to this issue in Section :ref:`sec_xicut`.

4. Combine all physical contributions of :eq:`eq_nellocomb_a` into :math:`\sigma^{(\ell)}(\xi_{c})` which has to be :math:`\xi_{c}` independent.

5. Perform detailed checks on :math:`\xi_{c}` independence.
   This is especially important on the first time a particular configuration is run.
   Beyond :term:`NLO`, it is also extremely helpful to check whether the sum of the fits :eq:`eq_xifit` is compatible with a constant, i.e. whether for all :math:`1\le i\le\ell`

   .. math::
      :label: eq_xifitsum

      \Bigg|
        \frac{\sum_{j=0}^\ell        c_i^{(j)} }
             {\sum_{j=0}^\ell \delta c_i^{(j)} }
      \Bigg| < 1

   where :math:`\delta c_i^{(j)}` is the error estimate on the coefficient :math:`c_i^{(j)}`.\ [2]_
   pymule's :func:`~pymule.mergefkswithplot` can be helpful here.

   If :eq:`eq_xifitsum` is not satisfied or only very poorly, try to run the Monte Carlo again with an increased :math:`n`.

6. Merge the different estimates of :eq:`eq_nellocomb_a` from the different :math:`\xi_{c}` into one final number :math:`\sigma^{(\ell)}`.
   The :math:`\chi^2/{\rm d.o.f.}` of this merging must be small.

7. Repeat the above for any distributions produced, though often bin-wise fitting as in Point 3 is rarely necessary or helpful.

   If a total cross section is :math:`\xi_{c}` independent but the distributions (or a cross section obtained after applying cuts) are not, this is a hint that the distribution (or the applied cuts) is not :term:`IR` safe.

These steps have been almost completely automatised in pymule and Mathematica.
Though all steps of this pipeline could be easily implemented in any other language by following the specification of the file format below (Section :ref:`sec_vegasff`).


.. _sec_compilation:

Manual compilation
------------------

You might need to compile McMule manually if you are not using a sufficiently recent Linux distribution or want to work it on yourself.
In this case, you first need to obtain a copy of the McMule source code.
We recommend the following approach

.. code:: bash

   $ git clone --recursive https://gitlab.com/mule-tools/mcmule

To build McMule, you will need

 * Python 3.8 or newer
 * `Meson <https://mesonbuild.com/>`_ 0.64.0 or newer
 * `ninja <https://ninja-build.org>`_ 1.8.2 or newer
 * GFortran 4.8 or newer

Now you need to configure and build McMule using meson and ninja

.. code:: bash

   $ meson setup build
   $ ninja -C build

Note that this will distribute the build on as many CPUs as your machine has which can cause memory issues.
If you do not want to do that, add ``-j <number of jobs>`` flag to the ninja command.
Despite the parallelisation, a full build of McMule is can take up to 1h, depending on your machine.
If you only need to compile some parts of McMule (such as Bhabha scattering), you can control which :term:`process groups<process group>` are build

.. code:: bash

   $ meson setup build -Dgroups=mue,ee

If you need debug symbols, you can disable optimisation

.. code:: bash

   $ meson setup build --buildtype=debug

Alternatively, we provide a Docker :term:`container` :cite:`Merkel:2014` for easy deployment and legacy results (cf. Section :ref:`sec_docker`).
In multi-user environments, *udocker* :cite:`Gomes:2017hct` can be used instead.
In either case, a pre-compiled copy of the code can be obtained by calling

.. code:: bash

   $ docker pull registry.gitlab.com/mule-tools/mcmule  # requires Docker to be installed
   $ udocker pull registry.gitlab.com/mule-tools/mcmule # requires uDocker to be installed

Running in a container
^^^^^^^^^^^^^^^^^^^^^^
Linux :term:`containers<container>` are an emergent new technology in the software engineering world.
The main idea behind such :term:`containerisation` is to bundle all dependencies with a software when shipping.
This allows the software to be executed regardless of the Linux distribution running on the host system without having to install any software beyond the containerising tool.
This is possible without any measurable loss in performance.
For these reasons, containerising McMule allows the code to be easily deployed on any modern computer, including systems running macOS or Windows (albeit with a loss of performance), and all results to be perfectly reproducible.

A popular :term:`containerisation` tool is Docker :cite:`Merkel:2014`.
Unfortunately, Docker requires some processes to be executed in privileged mode which is rarely available in the multi-user environments usually found on computing infrastructures.
This led to the creation of *udocker* :cite:`Gomes:2017hct` which circumvents these problems.

*udocker* can be installed by calling

.. warning::
    It might be advisable to point the variable ``UDOCKER_DIR`` to a folder on a drive without quota first as *udocker* requires sizeable disk space

.. code:: bash

   $ curl https://raw.githubusercontent.com/indigo-dc/udocker/master/udocker.py > udocker
   $ chmod u+rx ./udocker
   $ ./udocker install

Once Docker or *udocker* has been installed, McMule can be downloaded by simply calling

.. code:: bash

   $ docker pull yulrich/mcmule  # requires Docker to be installed
   $ udocker pull yulrich/mcmule # requires udocker to be installed

This automatically fetches the latest public release of McMule deemed stable by the McMule collaboration.
We will discuss some technical details behind :term:`containerisation` in Section :ref:`sec_docker`.

McMule can be run containerised on a specified ``user.f95`` which is compiled automatically into ``mcmule``.
This is possible both directly or using :term:`menu files<menu file>` as discussed above.
To run McMule directly on a specified ``user.f95``, simply call

.. code:: bash

   $ ./tools/run-docker.sh -i yulrich/mcmule:latest -u path/to/user.f95  -r

This requests the same input already discussed in :numref:`tab_mcmuleinput`.
To run a :term:`containerised<container>` :term:`menu file`, add an ``image`` command before the first ``conf`` command in the :term:`menu file`

.. code:: bash

   image yulrich/mcmule:latest path/to/user.f95
   conf babar-tau-e/m2enng-tau-e.conf

   run 70998 0.500000 m2enngR tau-e 0
   ...

Note that only one ``image`` command per :term:`menu file` is allowed.
After this, the :term:`menu file` can be executed normally though the drive where Docker or *udocker* is installed needs to be shared between all nodes working on the job.
It is recommended that all legacy results use be produced with *udocker* or Docker.


.. [1]
   Note that it is important to perform the fit after combining the phase-space partitionings (cf. Section :ref:`sec_ps`) but before adding :eq:`eq_nellocomb_a` as this model is only valid for the terms of :eq:`eq_nellocomb_b`

.. [2]
   Note that the error estimate on the sum of the total coefficients in :eq:`eq_xifitsum` is rather poor and does not include correlations between different :math:`c_i`.
