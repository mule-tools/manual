from docutils import nodes
from docutils.parsers.rst import Directive
from sphinx.util import logging
from pathlib import Path
import yaml


class Authors(Directive):
    required_arguments = 1
    footnotes = {}

    def to_unicode(self, expr):
        return expr.replace(r'\"u', "\u00fc")\
                   .replace(r'\`a', "\u00e0")\
                   .replace(r"\'a", "\u00e1")\
                   .replace(r'\"a', "\u00e4")\
                   .replace(r'\`e', "\u00e8")\
                   .replace(r"\'e", "\u00e9")\
                   .replace(r"\'i", "\u00ed")\
                   .replace(r"{\ss}", "\u00df")\
                   .replace(r"\&", "&")\
                   .replace(r"$^+$", "\u207A")


    def footnote(self, short, long):
        if short in self.footnotes:
            key = self.footnotes[short]['key']
        else:
            key = "affil"+str(len(self.footnotes))

            name = nodes.fully_normalize_name(key)
            footnote = nodes.footnote(auto=1)
            footnote['names'].append(name)
            footnote += nodes.paragraph(text=self.to_unicode(long))
            self.state_machine.document.note_autofootnote(footnote)
            self.state_machine.document.note_explicit_target(footnote, footnote)

            self.footnotes[short] = {"key": key, "obj": footnote}

        refname = nodes.fully_normalize_name(key)
        refnode = nodes.footnote_reference(f"[#{key}]_", refname=refname, auto=1)
        self.state_machine.document.note_autofootnote_ref(refnode)
        self.state_machine.document.note_footnote_ref(refnode)

        return refnode

    def run(self):
        self.logger = logging.getLogger(__name__)
        with open(Path(__file__).parent.parent.parent/self.arguments[0]) as fp:
            citation = yaml.unsafe_load(fp)

        self.affiliations = {
            i['short']: i['long']
            for i in citation['affiliations']
        }

        para = nodes.paragraph()
        for author in citation['authors']:
            para += nodes.Text(
                f"{author['given-names']} {author['family-names']} "
            )

            if 'orcid' in author:
                orcid = nodes.reference(refuri=author['orcid'])
                orcid += nodes.image(uri="_static/orcid.png", height="1em")
                para += orcid

            for affil in author['affiliation'].split(" & "):
                para += self.footnote(affil, self.affiliations[affil])
                para += nodes.Text(" ")
            para.pop()
            para += nodes.Text(", ")
        para.pop()

        return [para] + [i['obj'] for i in self.footnotes.values()]


def setup(app):
    app.add_directive("authors", Authors)
    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
