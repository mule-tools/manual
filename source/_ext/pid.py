from docutils import nodes
from docutils.parsers.rst import Directive
from docutils.parsers.rst import directives
from sphinx.util import logging
from sphinx.errors import ExtensionError
from pathlib import Path
import re


class PIDTable(Directive):
    has_content = True
    required_arguments = 1
    nmax = 7

    re_which_piece = re.compile(" *case\('(.*)'\)")
    re_set_func = re.compile(" *call set_func\('([01]*)', (.*)\)")
    re_ps = re.compile(" *ps => (.*) ; fxn => (.*)")
    re_n = re.compile(" *nparticle = (\d) ; ndim = (\d*)")
    re_masses = re.compile(" *masses\(\d*:\d*\) = \(/(.*)/\)")
    re_polarised = re.compile(" *polarised = (\d)")

    re_use = re.compile(" *use (.*), only: (.*)!!\((.*)\)")
    re_pid = re.compile(" *!! (.*->.*)")
    re_comment = re.compile(" *!! (.*)")

    def parse_integrands(self, file):
        pieces = {}
        it = iter(file.splitlines())
        line = ""
        try:
            while True:
                if m0 := self.re_which_piece.match(line):
                    piece = {
                        "mat_el": {},
                        "polarised": 0
                    }
                    for line in it:
                        if m := self.re_set_func.match(line):
                            piece["mat_el"][int(m.group(1), 2)] = m.group(2)
                        elif m := self.re_ps.match(line):
                            piece["ps"] = m.group(1)
                            piece["fxn"] = m.group(2)
                        elif m := self.re_n.match(line):
                            piece["nparticle"] = int(m.group(1))
                            piece["ndim"] = int(m.group(2))
                        elif m := self.re_masses.match(line):
                            piece["masses"] = [
                                i.strip()
                                for i in m.group(1).replace("._prec", "").split(",")
                            ]
                        elif m := self.re_polarised.match(line):
                            piece["polarised"] = int(m.group(1))
                        else:
                            break
                    pieces[m0.group(1)] = piece
                else:
                    line = next(it)
        except StopIteration:
            pass

        return pieces

    def parse_mat_el(self, file):
        mat_els = {}
        it = iter(file.splitlines())
        for line in it:
            if m0 := self.re_use.match(line):
                mat_el = {
                    "group": m0.group(1),
                    "args": [
                        i.strip().lower()
                        for i in m0.group(3).split(",")
                        if (not i.strip().startswith("pol")) and
                           (not i.strip().startswith("n")) and
                           (not i.strip() == "y") and
                           (not i.strip() == "z")
                    ],
                    "process": [],
                    "comment": []
                }
                for line in it:
                    if m := self.re_pid.match(line):
                        process = re.split("-*>", m.group(1))
                        sin = process[0]
                        sout = process[-1]

                        mat_el["process"].append((
                            re.split(" +", sin.strip().lower()),
                            re.split(" +", sout.strip().lower())
                        ))
                    elif m := self.re_comment.match(line):
                        mat_el["comment"].append(m.group(1))
                    else:
                        break
                mat_els[m0.group(2)] = mat_el

        return mat_els

    def mkparticle(self, p):
        return re.sub("([+-])$", r"^\1", p)\
            .replace("mu", r"\mu")\
            .replace("tau", r"\tau")\
            .replace("nu", r"\nu")\
            .replace("g", r"\gamma")

    def mktable(self, rows):
        tab = nodes.table("", classes=["longtable pidtable"])
        group = nodes.tgroup("", cols=2 + 2*self.nmax)
        tab.append(group)
        group.append(nodes.colspec(
            "", colwidth=5, colname="piece", align="left", colsep="yes"
        ))
        group.append(nodes.colspec(
            "", colwidth=5, colname="polarised", align="center", colsep="yes"
        ))
        for i in range(self.nmax):
            group.append(nodes.colspec(
                "", colwidth=5, colname=f"s{i}", align="center", colsep="no"
            ))
            group.append(nodes.colspec(
                "", colwidth=5, colname=f"t{i}", align="center", colsep="no"
            ))

        head = nodes.thead("")
        group.append(head)
        body = nodes.tbody("")
        group.append(body)

        # make header
        row = nodes.row("")
        row.append(nodes.entry("",nodes.literal(text="which_piece")))
        row.append(nodes.entry("",nodes.paragraph(text="P?")))
        for i in range(self.nmax):
            row.append(nodes.entry(
                "", nodes.math(text=f"p_{i+1}"),
                morecols=1
            ))

        head.append(row)

        buf = []
        prev_pid = None
        ds = False
        for piece, pol, pid in rows:
            if pid == prev_pid:
                row = nodes.row("")
                row.append(nodes.entry("",nodes.literal(text=piece)))
                row.append(nodes.entry("",nodes.paragraph(text=pol)))
                buf.append(row)
            else:
                if len(buf) > 0:
                    for c in buf[0].children[2:]:
                        c.attributes["morerows"] = len(buf)-1

                for row in buf:
                    body.append(row)

                buf = []

                prev_pid = pid

                if piece is None:
                    ds = True
                    continue

                row = nodes.row("")

                if ds:
                    ds = False
                    row.attributes['classes'].append("first")

                row.append(nodes.entry("",nodes.literal(text=piece)))
                row.append(nodes.entry("",nodes.paragraph(text=pol)))

                for p in pid[0][:-1]:
                    row.append(nodes.entry("",nodes.math(text=p), morecols=1))

                row.append(nodes.entry("",nodes.math(text=pid[0][-1])))
                row.append(nodes.entry("",nodes.math(text=r"\to")))

                it = iter(pid[1])
                for p in it:
                    if p[0] == "*":
                        pn = next(it)

                        if pn[0] != "*":
                            raise ExtensionError(
                                f"Average requires exactly two particles, found one in {piece}"
                            )

                        row.append(nodes.entry("",nodes.math(
                            text=rf"\big[{p[1:]}{pn[1:]}\big]"
                        ), morecols=3))
                    else:
                        row.append(nodes.entry("",nodes.math(text=p), morecols=1))

                for _ in range(self.nmax - len(pid[0]) - len(pid[1])):
                    row.append(nodes.entry("", morecols=1))

                buf.append(row)

        return tab

    def process1(self, piece):
        mat_el = self.mat_els[self.integrands[piece]["mat_el"][0].lower()]

        for pin, pout in mat_el["process"]:
            pidIn = []
            pidOut = []
            args = []
            for p in pin:
                if m := re.match("(.*)\((.*)\)", p):
                    pidIn.append(self.mkparticle(m.group(1)))
                    args.append(m.group(2))
                else:
                    pidIn.append("*"+self.mkparticle(p))
                    args.append(mat_el['args'][len(args)])
            for p in pout:
                if m := re.match("(.*)\((.*)\)", p):
                    pidOut.append(self.mkparticle(m.group(1)))
                    args.append(m.group(2))
                else:
                    pidOut.append("*"+self.mkparticle(p))
                    args.append(mat_el['args'][len(args)])

            if args != mat_el['args'][:len(args)]:
                self.logger.error(f"PID mismatch in {piece}: is {mat_el['args'][:len(args)]}, claims {args}")
                return (
                    piece, self.integrands[piece]["polarised"],
                    "error"
                )

        return (
            piece, self.integrands[piece]["polarised"],
            (pidIn, pidOut)
        )

    def run(self):
        self.logger = logging.getLogger(__name__)
        mcmule = Path(__file__).parent.parent.parent/self.arguments[0]

        try:
            self.integrands = self.parse_integrands(
                (mcmule / "src" / "integrands.f95").read_text()
            )
        except FileNotFoundError as ex:
            raise ExtensionError(
                f"Cannot find integrands.f95 at {self.arguments[0]}",
                ex
            )

        try:
            self.mat_els = self.parse_mat_el(
                (mcmule / "src" / "mat_el.f95").read_text()
            )
        except FileNotFoundError as ex:
            raise ExtensionError(
                f"Cannot find mat_el.f95 at {self.arguments[1]}",
                ex
            )

        rows = []
        for piece in self.content:
            if piece == "":
                continue
            if piece.startswith("--"):
                rows.append((None, None, None))
                continue

            rows.append(self.process1(piece))

        return [self.mktable(rows)]


def setup(app):
    app.add_directive("pid_table", PIDTable)
    app.add_css_file("pid.css")
    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
