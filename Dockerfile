FROM alpine:3.17
RUN apk add python3 py3-pip make rsvg-convert git
RUN echo "pi = 3.141592653589793" > /usr/lib/python3.10/numpy.py &&\
    echo -e "def cycler(*args, **kwargs):\n pass" > /usr/lib/python3.10/cycler.py &&\
    mkdir /usr/lib/python3.10/matplotlib/ &&\
    mkdir /usr/lib/python3.10/matplotlib/legend &&\
    touch /usr/lib/python3.10/matplotlib/patches.py &&\
    touch /usr/lib/python3.10/matplotlib/lines.py &&\
    touch /usr/lib/python3.10/matplotlib/pyplot.py &&\
    echo "PolyCollection = 0" > /usr/lib/python3.10/matplotlib/collections.py &&\
    echo "Path = None" > /usr/lib/python3.10/matplotlib/path.py &&\
    echo "_default_handler_map = [0]" > /usr/lib/python3.10/matplotlib/legend/Legend.py &&\
    echo -e "import matplotlib.legend.Legend\ndef rc(*args, **kwargs):\n pass" > /usr/lib/python3.10/matplotlib/__init__.py &&\
    mkdir /usr/lib/python3.10/mpl_toolkits/ &&\
    mkdir /usr/lib/python3.10/mpl_toolkits/axes_grid1/ &&\
    echo "Axes = 0" > /usr/lib/python3.10/mpl_toolkits/axes_grid1/mpl_axes.py &&\
    echo -e "Divider = None\nSize = None" > /usr/lib/python3.10/mpl_toolkits/axes_grid1/__init__.py &&\
    mkdir /usr/lib/python3.10/scipy/ &&\
    echo "optimize=0" > /usr/lib/python3.10/scipy/__init__.py &&\
    echo "" > /usr/lib/python3.10/scipy/stats.py
